import time,os
from scipy.integrate import quad
from scipy.special import genlaguerre,gamma,iv,ive
import numpy as np
from norm_generateLookupTable import NormIntegrand

# from S. Furuseth and X. Buffat, Eur. Phys. J. Plus 137, 506 (2022),
# https://doi.org/10.1140/epjp/s13360-022-02645-3

def genbinom(l,m):
    if l-m+1 > 0:
        return gamma(l+1)/(gamma(l-m+1)*gamma(m+1))
    else:
        return (-1)**(m)*gamma(m-l)/(gamma(m+1)*gamma(-l))

def normMavromatis(n1,l1,n2,l2):
    b1 = genbinom(n1+l1,n1)
    b2 = genbinom(n2+l2-(l1+l2)/2-1,n2)
    g = gamma((l1+l2)/2+1)
    try:
        hyp = hyp3f2(-n1,(l1+l2)/2+1,(l1+l2)/2-l2+1,l1+1,(l1+l2)/2-l2-n2+1,1)
    except ValueError:
        hyp = 0.0
    return b1*b2*g*float(str(hyp))

def normNumerical(n1,l1,n2,l2):
    myFunc = NormIntegrand(n1,l1,n2,l2)
    res,err = quad(myFunc,0,np.inf)
    return res

def isNonPositiveInteger(b):
    return b <= 0 and np.abs(np.floor(b+0.5)-b)<1E-6
        
# The norm can be computed using Appell F2 hypergoeometric function:
#return gamma(np.abs(l1)+1+n1)*gamma(np.abs(l2)+1+n2)/gamma(np.abs(l1)+1)/gamma(np.abs(l2)+1)/factorial(n1)/factorial(n2)*gamma((np.abs(l1)+np.abs(l2))/2+1)*appellf2((np.abs(l2)+np.abs(l1))/2+1,-n2,-n1,int(np.abs(l2))+1,int(np.abs(l1))+1,1,1)
# However the implementation of Appellf2 in mpmath does not support analytical continuation to x=y=1, leading to erratic behaviour.
def norm(eigvec):
    useLookupTable = False
    lookupTableFileName = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                       'normLookupTable.npy')
    if os.path.exists(lookupTableFileName):
        myFile = open(lookupTableFileName,'rb')
        lookupTable = np.load(myFile)
        myFile.close()
        useLookupTable = True
    if not useLookupTable:
        print("WARNING! Not using look-up table for eta computation!")
    (l0,n0) = np.shape(eigvec)
    lmax = int((l0-1) / 2)
    myNorm = 0.0
    sqrt8 = np.sqrt(8)
    for l1 in range(-lmax,lmax+1):
        for l2 in range(-lmax,lmax+1):
            fact = sqrt8**(np.abs(l1)+np.abs(l2))
            for n1 in range(n0):
                for n2 in range(n0):
                    if useLookupTable and np.abs(l1) < 50 and n1 < 50 and np.abs(l2) < 50 and n2 < 50:
                        myNorm += eigvec[l1+lmax,n1]*np.conj(eigvec[l2+lmax,n2])*lookupTable[np.abs(l1),n1,np.abs(l2),n2]/fact
                    elif l1 == l2:
                        if n1 == n2:
                            myNorm += eigvec[l1+lmax,n1]*np.conj(eigvec[l2+lmax,n2])*gamma(np.abs(l1)+n1+1)/gamma(n1+1)/fact
                    #elif not isNonPositiveInteger((l1+l2)/2-l2-n2+1): #requires mpmath
                    #    myNorm += eigvec[l1+lmax,n1]*np.conj(eigvec[l2+lmax,n2])*normMavromatis(n1,np.abs(l1),n2,np.abs(l2))/fact
                    #elif not isNonPositiveInteger((l1+l2)/2-l1-n1+1): #requires mpmath
                    #    myNorm += eigvec[l1+lmax,n1]*np.conj(eigvec[l2+lmax,n2])*normMavromatis(n2,np.abs(l2),n1,np.abs(l1))/fact
                    else:
                        myNorm += eigvec[l1+lmax,n1]*np.conj(eigvec[l2+lmax,n2])*normNumerical(n1,np.abs(l1),n2,np.abs(l2))/fact
    return np.sqrt(np.abs(myNorm))

def getDipoleNoiseSensitivity(eigvec,headtail_phase,myNorm = None):
    (l0,n0) = np.shape(eigvec)
    lmax = int((l0-1) / 2)
    eta0 = 0.0
    for l in range(-lmax,lmax+1):
        for n in range(n0):
            eta0 += eigvec[l+lmax,n]*(1j)**(-l)*np.sign(l)**np.abs(l)/(
                2**(2*np.abs(l)+n)*gamma(n+1))*headtail_phase**(np.abs(l)+2*n
                        )*np.exp(-0.5*headtail_phase**2)
    if myNorm == None:
        myNorm = norm(eigvec)
    return np.abs(eta0) / myNorm,myNorm

def getCrabAmplitudeNoiseSensitivity(eigvec,headtail_phase,myNorm):
    (l0,n0) = np.shape(eigvec)
    lmax = int((l0-1) / 2)
    eta1 = 0.0
    for l in range(-lmax,lmax+1):
        for n in range(n0):
            if headtail_phase == 0.0:
                if n == 0 and np.abs(l) == 1:
                    eta1 += np.sign(l)*eigvec[l+lmax,n]/4
            else:
                if l<0:
                    sign = (-1)**(l+1)
                else:
                    sign = 1
                eta1 += eigvec[l+lmax,n]*sign*(1j)**(l+1)/(2.0**(2*np.abs(l)+n)*
                    gamma(n+1))*headtail_phase**(2*n+np.abs(l)-1)*np.exp(
                        -0.5*headtail_phase**2)*(headtail_phase**2-np.abs(l)-2*n)
    if myNorm == None:
        myNorm = norm(eigvec)
    return np.abs(eta1) / myNorm,myNorm
