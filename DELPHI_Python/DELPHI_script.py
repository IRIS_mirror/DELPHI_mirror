import os,sys
import subprocess
# import local libraries if needed
pymod=subprocess.check_output("echo $PYMOD",shell=True).strip().decode()
if pymod.startswith('local'):
    py_numpy=subprocess.check_output("echo $PY_NUMPY",shell=True).strip().decode()
    sys.path.insert(1,py_numpy)
    py_scipy=subprocess.check_output("echo $PY_SCIPY",shell=True).strip().decode()
    sys.path.insert(1,py_scipy)
    py_matpl=subprocess.check_output("echo $PY_MATPL",shell=True).strip().decode()
    sys.path.insert(1,py_matpl)
from DELPHI import *
import pickle as pick
from pprint import pprint

# script encapsulating a DELPHI calculation with scans
# launched in command line with one argument, the name of the file containing
# all the parameters (pickle format)

def extract_scalar_from_array(ar):
    """
    Convert an array with zero dimension into a scalar. If the dimension of 
    ar is higher, we return it unmodified.
    """
    if not ar.shape:
        return ar.item()
    else:
        return ar

if __name__ == "__main__":

    if len(sys.argv)>1:
        filename=str(sys.argv[1])
    print(filename)

    listargs = ['Qpscan', 'nxscan', 'dampscan', 'Nbscan', 'omegasscan', 'dphasescan',
                'M','omega0', 'Q', 'gamma', 'eta', 'a', 'b', 'taub', 'g', 'Z', 'freq']
    param_dict = dict(np.load(filename,allow_pickle=True))
    print("Parameters:")

    for key,value in param_dict.items():
        if key in listargs:
            exec("{}=extract_scalar_from_array(value)".format(key))
            print("{} = {}".format(key,eval(key)))
        else:
            param_dict[key] = extract_scalar_from_array(value)
    for key in listargs:
        param_dict.pop(key)
    pprint(param_dict)

    # remove initial file
    os.system("rm -f "+filename)

    flagm0 = param_dict.get('flagm0',False)
    flageigenvect = param_dict.get('flageigenvect',False)

    result_dict = eigenmodesDELPHI_converged_scan(
                Qpscan, nxscan, dampscan, Nbscan, omegasscan, dphasescan, M,
                omega0, Q, gamma, eta, a, b, taub, g, Z, freq, **param_dict)

    print(result_dict['tuneshift_most'].shape,result_dict['tuneshiftnx'].shape)
    print(result_dict['tuneshift_most'])#,tuneshiftnx)
    if flagm0:
        print(result_dict['tuneshiftm0'].shape,result_dict['tuneshiftm0'])

    result_dict['tuneshiftnx'] = [] # 7/12/2013: I erase it because it is never used... (to save space in multibunch)
    # save results
    with open('out'+filename,'wb') as fileall:
        pick.dump(result_dict,fileall)
