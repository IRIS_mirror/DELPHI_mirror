import time
from scipy.integrate import quad
from scipy.special import genlaguerre,gamma
from math import factorial
import numpy as np

class NormIntegrand:
    def __init__(self,n1,l1,n2,l2):
        self.l1 = l1
        self.l2 = l2
        self.lag1 = genlaguerre(n1,l1)
        self.lag2 = genlaguerre(n2,l2)

    def __call__(self,x):
        return x**((self.l1+self.l2)/2)*np.exp(-x)*self.lag1(x)*self.lag2(x)

if __name__ == '__main__':
    lmax = 50
    nmax = 50
    values = np.zeros((nmax,lmax,nmax,lmax))

    time0 = time.time()
    for l1 in range(0,lmax):
        for l2 in range(0,lmax):
            print(l1,l2)
            for n1 in range(0,nmax):
                for n2 in range(0,nmax):
                    if l1 == l2:
                        if n1 == n2:
                            values[l1,n1,l2,n2] = gamma(l1+n1+1)/factorial(n1)
                    else:
                        myFunc = NormIntegrand(n1,l1,n2,l2)
                        values[l1,n1,l2,n2],err = quad(myFunc,0,np.inf)
    print(time.time()-time0)

    myFile = open('normLookuptable.npy','wb')
    np.save(myFile,values)
    myFile.close()
