import time,os
from scipy.integrate import quad
from scipy.special import genlaguerre,gamma,iv,ive
import numpy as np
from norm_generateLookupTable import NormIntegrand
from Noise import getDipoleNoiseSensitivity,getCrabAmplitudeNoiseSensitivity

from matplotlib import pyplot as plt

# from S. Furuseth and X. Buffat, Eur. Phys. J. Plus 137, 506 (2022),
# https://doi.org/10.1140/epjp/s13360-022-02645-3


if __name__ == '__main__':
    # Here we compare eta0 (for dipole noise) with the approximated eta (eq. 22
    # from the reference above).
    # Note that several radial modes should be summed to get an accurate result.
    sigmaz = 0.09
    eta = 0.000348331498127
    R = 26.65888319999888E3/(2.0*np.pi)
    chromas = np.arange(-20.0,20.1,1.0)
    for eigvec,l,n in zip([np.array([[1,0,0],[0,0,0],[0,0,0]]),
                           np.array([[0,1,0],[0,0,0],[0,0,0]]),
                           np.array([[0,0,1],[0,0,0],[0,0,0]]),
                           np.array([[0,0,0],[1,0,0],[0,0,0]]),
                           np.array([[0,0,0],[0,1,0],[0,0,0]]),
                           np.array([[0,0,0],[0,0,1],[0,0,0]]),
                           np.array([[0,0,0],[0,0,0],[1,0,0]]),
                           np.array([[0,0,0],[0,0,0],[0,1,0]]),
                           np.array([[0,0,0],[0,0,0],[0,0,1]]),
                           ],
                          [-1,-1,-1,0,0,0,1,1,1],
                          [ 0, 1, 2,0,1,2,0,1,2]):
        if n==0:
            eta0s = np.zeros_like(chromas)
            eta0s_approx = np.zeros_like(chromas)
            eta1s = np.zeros_like(chromas)
        print("="*60)
        print("l={}".format(l))
        print(eigvec)
        print("="*60)
        for iChroma,chroma in enumerate(chromas):
            headtail_phase = chroma*sigmaz/(eta*R)
            eta0,myNorm = getDipoleNoiseSensitivity(eigvec,headtail_phase)
            eta1,myNorm = getCrabAmplitudeNoiseSensitivity(eigvec,headtail_phase,myNorm)
            if n==0:
                eta0s_approx[iChroma] = np.abs(ive(l,headtail_phase**2)) # ive(l,x)=np.exp(-x)*iv(l,x)
            eta0s[iChroma] += abs(eta0)**2
            eta1s[iChroma] += abs(eta1)**2
            #print(chroma,eta0s[iChroma],eta0s_approx[iChroma],eta1s[iChroma])
        if n==2:
            plt.figure(0)
            plt.plot(chromas,eta0s)
            plt.plot(chromas,eta0s_approx,'x')
            plt.figure(1)
            plt.plot(chromas,eta1s)

    plt.show()


