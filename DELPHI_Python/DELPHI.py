# Library for computation of instabilities from the DELPHI code

import sys
import subprocess

# Define user (for bsub command) (environment variable USER should be previously defined)
user = subprocess.check_output('echo $USER',shell=True).strip().decode()
if len(user) > 0:
    user_option = ' -u '+user
else:
    user_option = ''

# import local libraries if needed
pymod = subprocess.check_output('echo $PYMOD',shell=True).strip().decode()
if pymod.startswith('local'):
    py_numpy = subprocess.check_output('echo $PY_NUMPY',shell=True).strip().decode()
    sys.path.insert(1, py_numpy)
    py_scipy = subprocess.check_output('echo $PY_SCIPY',shell=True).strip().decode()
    sys.path.insert(1, py_scipy)
    py_matpl = subprocess.check_output('echo $PY_MATPL',shell=True).strip().decode()
    sys.path.insert(1, py_matpl)

import numpy as np
from copy import deepcopy
from string import *
import os
import re
from collections import defaultdict
from io_lib import read_ncol_file, write_ncol_file
from string_lib import fortran_str, float_to_str
from tables_lib import *
from C_complex import *
from particle_param import *
from ctypes import *
from numpy import linalg as li
import time as ti
import inspect
from math import factorial
import scipy.special
from scipy.special import genlaguerre,eval_genlaguerre
from scipy.interpolate import interp1d
from Noise import getDipoleNoiseSensitivity,getCrabAmplitudeNoiseSensitivity

clight=299792458 # speed of light in m/s

try:
    libDELPHI = CDLL('libDELPHI.so')
except Exception:
    libDELPHI = CDLL('/'.join(__file__.split('/')[:-2])+'/DELPHI/libDELPHI.so')

# c_double is the type of the result
prototypefact = CFUNCTYPE(c_double, c_int, c_int)
factpart = prototypefact(('factorialpart', libDELPHI))

# 1st c_double is the type of the result
prototypelag = CFUNCTYPE(c_double, c_int, c_int, c_double)
laguerre = prototypelag(('Laguerre', libDELPHI))


class Laguerre(object):
    """
    Class to deal with Laguerre polynomials.
    All a & b here are the bare ones, i.e. NOT divided by taub^2.
    """
    
    def __init__(self,a,b,taub,g,omega):
        
        self.a = a
        self.b = b
        self.taub = taub
        self.g = g
        self.omega = omega
        self.omegataub = omega*taub
        omegataub2 = self.omegataub*self.omegataub/4.
        self.x = omegataub2/a
        if a==b:
            self.y = self.x
        else:
            self.y = omegataub2/b
    
    def factorialpart(self,n,k):
        res = 1
        for i in range(n,k,-1):
            res *= i
        return res

    def Iln(self,l,n):
        absl = abs(l)

        if self.a==self.b:
            signl = 1. if l>=0 or absl%2==0 else -1.
            coef = signl/(2**n*(2.*self.a)**(n+absl)*factorial(n))
            return np.complex128(coef*self.omegataub**(2*n+absl))

        return np.sign(l)**absl*(self.omegataub/(2.*self.b))**absl* \
                (1.-self.a/self.b)**n * \
                eval_genlaguerre(n,absl,self.a*self.y/(self.a-self.b))

    def Gln(self,l,n):
        res = 0.
        absl = abs(l)
        epsl = np.sign(l)**absl

        for k,the_g in enumerate(self.g):

            if k<n:
                fact = self.factorialpart(n,k)
                lag1 = genlaguerre(k,n-k)*np.poly1d((-1.,0.))**(n-k)/fact

            else:
                lag1 = genlaguerre(n,k-n)

            if n+absl<k:
                fact = self.factorialpart(k,n+absl)
                lag2 = genlaguerre(n+absl,k-n-absl)*np.poly1d((-1.,0.))**(k-n-absl)/fact

            else:
                lag2 = genlaguerre(k,n+absl-k)

            res += the_g*(-1)**(n+k)*lag1(self.x)*lag2(self.x)

        return res*epsl*self.omegataub**absl


def getstatusoutput(cmd):
    '''
    This emulates the obsolete function "commands.getstatusoutput"
    '''
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    out, _ = process.communicate()
    return (process.returncode, out.strip().decode())


def compute_impedance_matrix(
        lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
        a, b, taub, g, Z, freqZ, flag_trapz=0, abseps=1e-4,eps=1e-8,
        lmaxold=-1, nmaxold=-1, max_freq_old=0., couplold=None):
    """Compute DELPHI impedance matrix."""

    aovertaub2 = a/(taub*taub)
    bovertaub2 = b/(taub*taub)

    Z_interp = interp1d(freqZ,Z,axis=0,bounds_error=False,fill_value=(0.,0.))
    
    def impedance(freq):
        the_Z = Z_interp(np.abs(freq))
        freq_pos = np.where(freq>0)[0]
        freq_neg = np.where(freq<=0)[0]
        return np.hstack((-the_Z[freq_neg,0]+1j*the_Z[freq_neg,1],
                           the_Z[freq_pos,0]+1j*the_Z[freq_pos,1]))

    def weighted_impedance(min_freq,max_freq):
        """
        Function to compute the impedance weighted by a few of the factors that
        are in the final impedance sum (those independent from l & n)
        Compute from min_freq to max_freq, and -max_freq to -min_freq
        (note: these are frequencies in Hz, NOT angular frequencies).
        """
        Momega0 = M*omega0
        pmin = max(0,np.ceil((min_freq*2*np.pi-(nx+tunefrac)*omega0)/Momega0))
        pmax = np.floor((max_freq*2*np.pi-(nx+tunefrac)*omega0)/Momega0)
        if pmin==0:
            omegap = (nx+tunefrac)*omega0+Momega0*np.arange(-pmax,pmax+1)
        else:
            omegap = (nx+tunefrac)*omega0+Momega0*np.hstack(
                    (np.arange(-pmax,-pmin+1),np.arange(pmin,pmax+1)))
        omega = omegap-omegaksi
        omegataub = omega*taub
        omegataub2 = omegataub*omegataub/4.
        x = omegataub2/a
        y = omegataub2/b
        the_Z = np.exp(-x-y)*impedance(omegap/(2.*np.pi))/(2.*bovertaub2)
        return omega, the_Z

    def partial_matrix(min_freq,max_freq):
        # Compute the matrix using the part of the impedance that lies between
        # min_freq to max_freq (frequencies in Hz)
        delta_coupl = np.zeros((2*lmax+1, nmax+1, 2*lmax+1, nmax+1), dtype=complex)

        omega, the_Z = weighted_impedance(min_freq,max_freq)
        lag = Laguerre(a,b,taub,g,omega)

        if max_freq_old <= max_freq and couplold is not None:
            if min_freq <= max_freq_old:
                omega_old, the_Z_old = weighted_impedance(max_freq_old,max_freq)
            else:
                omega_old, the_Z_old = weighted_impedance(min_freq,max_freq)
            lag_old = Laguerre(a,b,taub,g,omega_old)

        I_lprimenprime,I_lprimenprime_old = {},{}
        for l in range(lmax, -lmax-1, -1):
            coefj = {}
            for n in range(nmax+1):
                G_ln = lag.Gln(l,n)
                # Compute factorial(n)/(factorial(n+|l|)*2^|l|)
                fact = 1. / (lag.factorialpart(n+abs(l), n)*2.**abs(l))
                Z_times_Gln = the_Z*G_ln

                if max_freq_old <= max_freq and couplold is not None:
                    G_ln_old = lag_old.Gln(l,n)
                    Z_times_Gln_old = the_Z_old*G_ln_old
                
                for lprime in range(lmax, -lmax-1, -1):
                    if lprime not in coefj:
                        coefj[lprime] = 1j**(lprime-l)

                    for nprime in range(nmax+1):
                        
                        if (l >= 0) and (lprime >= 0):

                            if ((couplold is None) or (l > lmaxold or lprime > lmaxold)
                                or (n > nmaxold or nprime > nmaxold)):

                                if (lprime,nprime) not in I_lprimenprime:
                                    I_lprimenprime[(lprime,nprime)] = lag.Iln(lprime,nprime)

                                Zsum = (Z_times_Gln.reshape((1,-1))).dot(
                                            I_lprimenprime[(lprime,nprime)].reshape((-1,1))).squeeze()
                                # the above the impedance sum (through a contracted product in numpy)
                                #print(l,n,lprime,nprime,Zsum.real,Zsum.imag)

                            elif (max_freq_old <= max_freq and couplold is not None):
                                # in this case we are within the range of couplold, but with a
                                # too small max_freq_old -> we compute what is missing
                                if (lprime,nprime) not in I_lprimenprime_old:
                                    I_lprimenprime_old[(lprime,nprime)] = lag_old.Iln(lprime,nprime)

                                Zsum = (Z_times_Gln_old.reshape((1,-1))).dot(
                                            I_lprimenprime_old[(lprime,nprime)].reshape((-1,1))).squeeze()

                            else:
                                Zsum = 0.+0.*1j

                            delta_coupl[l+lmax, n, lprime+lmax, nprime] = coefj[lprime]*fact*(Zsum.real + 1j*Zsum.imag)

                        # Other cases: uses the matrix symmetries
                        elif (l < 0) and (lprime >= 0):
                            delta_coupl[l+lmax, n, lprime+lmax, nprime] = delta_coupl[-l+lmax, n, lprime+lmax, nprime]

                        elif (l >= 0) and (lprime < 0):
                            delta_coupl[l+lmax, n, lprime+lmax, nprime] = delta_coupl[l+lmax, n, -lprime+lmax, nprime]

                        elif (l<0) and (lprime<0):
                            delta_coupl[l+lmax, n, lprime+lmax, nprime] = delta_coupl[-l+lmax, n, -lprime+lmax, nprime]

        return delta_coupl

    Momega0 = M*omega0
    twopi = 2*np.pi

    max_num_nodes = 2*nmax+lmax
    stepmin = max(20,np.floor(np.sqrt(2.*aovertaub2*max_num_nodes)/Momega0)+
        np.floor(omegaksi/Momega0))

    coupl = np.zeros((2*lmax+1, nmax+1, 2*lmax+1, nmax+1), dtype=complex)
    if (couplold is not None):
        if lmax>=lmaxold:
            coupl[lmax-lmaxold:lmax+lmaxold+1,0:min(nmax,nmaxold)+1,
                  lmax-lmaxold:lmax+lmaxold+1,0:min(nmax,nmaxold)+1] = (
                couplold[:,0:min(nmax,nmaxold)+1,:,0:min(nmax,nmaxold)+1])
        else:
            coupl[:,0:min(nmax,nmaxold)+1,:,0:min(nmax,nmaxold)+1] = (
                couplold[lmaxold-lmax:lmaxold+lmax+1,0:min(nmax,nmaxold)+1,
                         lmaxold-lmax:lmaxold+lmax+1,0:min(nmax,nmaxold)+1])
    delta_coupl = np.ones((2*lmax+1, nmax+1, 2*lmax+1, nmax+1), dtype=complex)*(1+1j)*1e50
    deltareal = np.abs(delta_coupl.real)
    deltaimag = np.abs(delta_coupl.imag)

    if lmax>lmaxold or nmax>nmaxold:
        min_freq = 0.
    else:
        min_freq = max_freq_old

    max_freq = max(min_freq,(nx+tunefrac)*omega0/twopi)

    while np.any(np.logical_or(
            np.logical_and(deltareal>abseps,deltareal>eps*np.abs(coupl.real)),
            np.logical_and(deltaimag>abseps,deltaimag>eps*np.abs(coupl.imag)))):

        max_freq = min_freq + (stepmin/twopi
                    + max_freq/(10.*Momega0+abs(omegaksi)) ) * Momega0
        delta_coupl = partial_matrix(min_freq,max_freq)
        coupl += delta_coupl
        last_max_freq = min_freq # that's the max_freq of the previous step
        min_freq = max_freq
        deltareal = np.abs(delta_coupl.real)
        deltaimag = np.abs(delta_coupl.imag)

    # return matrix and the max_freq of the previous step (if we return max_freq it will keep increasing for nothing)
    return coupl,last_max_freq


def compute_impedance_matrix_old(
        lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
        a, b, taub, g, Z, freqZ, flag_trapz=0, abseps=1e-4,
        lmaxold=-1, nmaxold=-1, couplold=None):
    """Compute DELPHI impedance matrix."""

    aovertaub2 = a / (taub*taub)
    bovertaub2 = b / (taub*taub)
    Zsum = Complex(0., 0.)

    ng = len(g)
    nZ = len(freqZ)
    nZ2 = 2 * nZ

    glist = g.tolist()
    flist = freqZ.tolist()
    Z2 = Z.reshape((1,-1))[0]
    Zlist = Z2.tolist()

    DArrayg = c_double * ng
    DArrayf = c_double * nZ
    DArrayZ = c_double * nZ2

    g_arr = DArrayg(*glist)
    Z_arr = DArrayZ(*Zlist)
    freq_arr = DArrayf(*flist)

    # C function prototype
    prototypeisum = CFUNCTYPE(Complex, c_int, c_int, c_double, c_double,
                              c_double, c_int, c_int, c_int, c_int, c_double,
                              c_double, c_double, DArrayg, c_long, DArrayZ,
                              DArrayf, c_long, c_int, c_double)
    isum = prototypeisum(('impedancesum', libDELPHI))

    # Complex is the type of the result
    coupl = np.zeros((2*lmax+1, nmax+1, 2*lmax+1, nmax+1), dtype=complex)

    # Compute the matrix
    for l in range(lmax, -lmax-1, -1):
        for lprime in range(lmax, -lmax-1, -1):
            coefj = 1j**(lprime-l)
            for n in range(nmax+1):
                # Compute factorial(n)/(factorial(n+|l|)*2^|l|)
                fact = 1. / (factpart(n+abs(l), n)*2.**abs(l))
                for nprime in range(nmax+1):

                    if (l >= 0) and (lprime >= 0):

                        if ((couplold is None)
                            or (((l > lmaxold) or (lprime > lmaxold))
                                or ((n > nmaxold) or (nprime > nmaxold)))):
                            # Matrix coefficient was never computed for this l, lprime, n, nprime
                            Zsum = isum(nx, M, c_double(omegaksi), c_double(omega0),
                                        c_double(tunefrac), l, lprime, n, nprime,
                                        c_double(aovertaub2), c_double(bovertaub2),
                                        c_double(taub), g_arr, c_long(ng), Z_arr,
                                        freq_arr, c_long(nZ), flag_trapz, c_double(abseps))
                            # print(Zsum.real,Zsum.imag)

                            coupl[l+lmax, n, lprime+lmax, nprime] = coefj*fact*(Zsum.real + 1j*Zsum.imag)

                        else:
                            # Matrix coefficient was already computed
                            coupl[l+lmax, n, lprime+lmax, nprime] = couplold[l+lmaxold, n, lprime+lmaxold, nprime]

                    # Other cases: uses the matrix symmetries
                    elif (l < 0) and (lprime >= 0):
                        coupl[l+lmax, n, lprime+lmax, nprime] = coupl[-l+lmax, n, lprime+lmax, nprime]

                    elif (l >= 0) and (lprime < 0):
                        coupl[l+lmax, n, lprime+lmax, nprime] = coupl[l+lmax, n, -lprime+lmax, nprime]

                    elif (l<0) and (lprime<0):
                        coupl[l+lmax, n, lprime+lmax, nprime] = coupl[-l+lmax, n, -lprime+lmax, nprime]

    return coupl


def compute_damper_matrix(lmax,nmax,nx,M,omegaksi,omega0,tunefrac,a,b,taub,g,flagdamperimp=0,d=None,freqd=None,abseps=1e-4,lmaxold=-1,nmaxold=-1,damperold=None):

    """Compute DELPHI damper matrix."""

    aovertaub2=a/(taub*taub)
    bovertaub2=b/(taub*taub)
    damp=Complex(0.,0.)

    ng=len(g)
    glist=g.tolist()
    DArrayg=c_double * ng
    g_arr=DArrayg(*glist)

    if ((flagdamperimp==1)and(d is not None))and(freqd is not None):

        nd=len(freqd)
        nd2=2*nd
        flist=freqd.tolist()
        d2=d.reshape((1,-1))[0]
        dlist=d2.tolist()

        DArrayf=c_double * nd
        DArrayd=c_double * nd2

        d_arr=DArrayd(*dlist)
        freq_arr=DArrayf(*flist)

        # C function prototype
        prototypedsum = CFUNCTYPE(Complex, c_int, c_int, c_double, c_double, c_double, c_int, c_int,
            c_int, c_int, c_double, c_double, c_double, DArrayg, c_long, DArrayd, DArrayf, c_long,
            c_double) # Complex is the type of the result
        dsum = prototypedsum(('dampersum', libDELPHI))

    else:

        # C function prototypes
        prototypeGln = CFUNCTYPE(c_double, c_int, c_int, c_double, c_double, c_double, c_long, DArrayg)
        Gln = prototypeGln(('Gln', libDELPHI))

        prototypeIln = CFUNCTYPE(c_double, c_int, c_int, c_double, c_double, c_double, c_double)
        Iln = prototypeIln(('Iln', libDELPHI))


    damper=np.zeros((2*lmax+1,nmax+1,2*lmax+1,nmax+1),dtype=complex)

    # compute matrix
    for l in range(lmax,-lmax-1,-1):

        for lprime in range(lmax,-lmax-1,-1):

            coefj=1j**(lprime-l)

            for n in range(nmax+1):

                fact=1./(factpart(n+abs(l),n)*2.**abs(l)) # =factorial(n)/(factorial(n+|l|)*2^|l|)

                for nprime in range(nmax+1):

                    if ( (l>=0)and(lprime>=0) ):

                        if (damperold is None) or ( ( (l>lmaxold)or(lprime>lmaxold) ) or ( (n>nmaxold)or(nprime>nmaxold) ) ):

                            # matrix coefficient was never computed for this l, lprime, n, nprime

                            if (flagdamperimp==1):
                                # damper from an impedance-like function
                                damp=dsum(nx, M, c_double(omegaksi), c_double(omega0),
                                        c_double(tunefrac), l, lprime, n, nprime, c_double(aovertaub2),
                                        c_double(bovertaub2), c_double(taub), g_arr, c_long(ng), d_arr, freq_arr, c_long(nd),
                                        c_double(abseps))

                                #print(damp.real,damp.imag)

                                damper[l+lmax,n,lprime+lmax,nprime]=coefj*fact*(damp.real+1j*damp.imag)


                            else:
                                # bunch-by-bunch damper
                                I_lprimenprime=Iln(lprime,nprime,c_double(-omegaksi),c_double(aovertaub2),c_double(bovertaub2),c_double(taub))
                                G_ln=Gln(l,n,c_double(-omegaksi),c_double(aovertaub2),c_double(taub),c_long(ng),g_arr)
                                damper[l+lmax,n,lprime+lmax,nprime]=coefj*fact*I_lprimenprime*G_ln

                        else:

                            # matrix coefficient was already computed
                            damper[l+lmax,n,lprime+lmax,nprime]=damperold[l+lmaxold,n,lprime+lmaxold,nprime]

                    # other cases: uses the matrix symmetries
                    elif ( (l<0)and(lprime>=0) ):

                        damper[l+lmax,n,lprime+lmax,nprime]=damper[-l+lmax,n,lprime+lmax,nprime]

                    elif ( (l>=0)and(lprime<0) ):

                        damper[l+lmax,n,lprime+lmax,nprime]=damper[l+lmax,n,-lprime+lmax,nprime]

                    elif ( (l<0)and(lprime<0) ):

                        damper[l+lmax,n,lprime+lmax,nprime]=damper[-l+lmax,n,-lprime+lmax,nprime]

    return damper


def compute_impedance_diagonal(
        lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
        a, b, taub, g, Z, freqZ, flag_trapz=0, abseps=1e-4,
        lmaxold=-1, nmaxold=-1, diagold=None):
    """Compute diagonal terms of DELPHI impedance matrix."""

    aovertaub2 = a / (taub*taub)
    bovertaub2 = b / (taub*taub)
    Zsum = Complex(0., 0.)

    ng = len(g)
    nZ = len(freqZ)
    nZ2 = 2 * nZ

    glist = g.tolist()
    flist = freqZ.tolist()
    Z2 = Z.reshape((1,-1))[0]
    Zlist = Z2.tolist()

    DArrayg = c_double * ng
    DArrayf = c_double * nZ
    DArrayZ = c_double * nZ2

    g_arr = DArrayg(*glist)
    Z_arr = DArrayZ(*Zlist)
    freq_arr = DArrayf(*flist)

    # C function prototype
    prototypeisum = CFUNCTYPE(Complex, c_int, c_int, c_double, c_double,
                              c_double, c_int, c_int, c_int, c_int, c_double,
                              c_double, c_double, DArrayg, c_long, DArrayZ,
                              DArrayf, c_long, c_int, c_double)
    isum = prototypeisum(('impedancesum', libDELPHI))

    # Complex is the type of the result
    diag = np.zeros((2*lmax+1, nmax+1), dtype=complex)

    # Compute the matrix diagonal
    for l in range(lmax, -lmax-1, -1):
        coefj = 1.
        for n in range(nmax+1):
            # Compute factorial(n)/(factorial(n+|l|)*2^|l|)
            fact = 1. / (factpart(n+abs(l), n)*2.**abs(l))

            if l>=0:

                if (diagold is None or (l > lmaxold) or (n > nmaxold)):
                    # Diagonal coefficient was never computed for this l, n
                    Zsum = isum(nx, M, c_double(omegaksi), c_double(omega0),
                                c_double(tunefrac), l, l, n, n,
                                c_double(aovertaub2), c_double(bovertaub2),
                                c_double(taub), g_arr, c_long(ng), Z_arr,
                                freq_arr, c_long(nZ), flag_trapz, c_double(abseps))
                    # print(Zsum.real,Zsum.imag)

                    diag[l+lmax, n] = coefj*fact*(Zsum.real + 1j*Zsum.imag)

                else:
                    # Matrix coefficient was already computed
                    diag[l+lmax, n] = diagold[l+lmaxold, n]

            # Otherwise uses the matrix symmetries
            else:
                diag[l+lmax, n] = diag[-l+lmax, n]

    return diag


def compute_damper_diagonal(lmax,nmax,nx,M,omegaksi,omega0,tunefrac,a,b,taub,g,flagdamperimp=0,d=None,freqd=None,abseps=1e-4,lmaxold=-1,nmaxold=-1,damperdiagold=None):

    """Compute DELPHI damper diagonal terms."""

    aovertaub2=a/(taub*taub)
    bovertaub2=b/(taub*taub)
    damp=Complex(0.,0.)

    ng=len(g)
    glist=g.tolist()
    DArrayg=c_double * ng
    g_arr=DArrayg(*glist)

    if ((flagdamperimp==1)and(d!=None))and(freqd!=None):

        nd=len(freqd)
        nd2=2*nd
        flist=freqd.tolist()
        d2=d.reshape((1,-1))[0]
        dlist=d2.tolist()

        DArrayf=c_double * nd
        DArrayd=c_double * nd2

        d_arr=DArrayd(*dlist)
        freq_arr=DArrayf(*flist)

        # C function prototype
        prototypedsum = CFUNCTYPE(Complex, c_int, c_int, c_double, c_double, c_double, c_int, c_int,
            c_int, c_int, c_double, c_double, c_double, DArrayg, c_long, DArrayd, DArrayf, c_long,
            c_double) # Complex is the type of the result
        dsum = prototypedsum(('dampersum', libDELPHI))

    else:

        # C function prototypes
        prototypeGln = CFUNCTYPE(c_double, c_int, c_int, c_double, c_double, c_double, c_long, DArrayg)
        Gln = prototypeGln(('Gln', libDELPHI))

        prototypeIln = CFUNCTYPE(c_double, c_int, c_int, c_double, c_double, c_double, c_double)
        Iln = prototypeIln(('Iln', libDELPHI))


    damperdiag=np.zeros((2*lmax+1,nmax+1),dtype=complex)

    # compute matrix diagonal
    for l in range(lmax,-lmax-1,-1):

        coefj=1.

        for n in range(nmax+1):

            fact=1./(factpart(n+abs(l),n)*2.**abs(l)) # =factorial(n)/(factorial(n+|l|)*2^|l|)

            if l>=0:

                if (damperdiagold is None or (l>lmaxold) or (n>nmaxold)):

                    # diagonal coefficient was never computed for this l, n

                    if (flagdamperimp==1):
                        # damper from an impedance-like function
                        damp=dsum(nx, M, c_double(omegaksi), c_double(omega0),
                                c_double(tunefrac), l, l, n, n, c_double(aovertaub2),
                                c_double(bovertaub2), c_double(taub), g_arr, c_long(ng), d_arr, freq_arr, c_long(nd),
                                c_double(abseps))

                        #print(damp.real,damp.imag)
                        damperdiag[l+lmax,n]=coefj*fact*(damp.real+1j*damp.imag)


                    else:
                        # bunch-by-bunch damper
                        I_ln=Iln(l,n,c_double(-omegaksi),c_double(aovertaub2),c_double(bovertaub2),c_double(taub))
                        G_ln=Gln(l,n,c_double(-omegaksi),c_double(aovertaub2),c_double(taub),c_long(ng),g_arr)
                        damperdiag[l+lmax,n]=coefj*fact*I_ln*G_ln

                else:

                    # matrix coefficient was already computed
                    damperdiag[l+lmax,n]=damperdiagold[l+lmaxold,n]

            # otherwise uses the matrix symmetries
            else:
                damperdiag[l+lmax,n]=damperdiag[-l+lmax,n]

    return damperdiag


def damper_imp_Karliner_Popov(L0,L1,L2,tauf,R,Q,f0,f):

    ''' evaluate (to a multiplicative factor) "impedance" of a damper
    (Karliner-Popov, Nucl. Inst. Meth. Phys. Res. A 2005)

    L0: distance from pickup to kicker (m),
    L1: pickup-length (m),
    L2: kicker length (m),
    tauf: 1/tauf ~ frequency of low-pass filter,
    R: machine radius,
    Q: tune,
    f0: revolution frequency,
    f: array of frequencies at which impedance is evaluated. '''

    c=299792458

    omega0=2.*np.pi*f0
    omega=2.*np.pi*f

    tau=(L0-2.*L2)/c

    # tmp=gamma_m-j*m/R
    tmp=1j*(-2.*omega/omega0-Q)/R

    # filter
    K=1./(1.-1j*omega*tauf)

    Zimp=K*np.exp(-1j*omega0*Q*tau)*(1.-np.exp(-tmp*L1))*(1.-np.exp(-tmp*L2))/tmp

    return Zimp


def computes_coef(f0,dmax,b,g0,dnormfactor,taub,dphase,M,Nb,gamma,Q,particle='proton'):

    ''' compute coefficients in front of damper and imepdance matrices in the final eigenvalue system

    - f0: revolution frequency,
    - dmax: damper gain (inverse of number of damping turns) - depends also on normalization factor dnormfactor,
    - b: b parameter in DELPHI (for Laguerre poly. decomposition),
    - g0: first term in distribution decomposition over Laguerre polynomial,
    - dnormfactor: normalization factor for damper matrix. Usually such that damping rate of mode nx=0, l=0
        and n=0 (i.e. all coupled-bunch, azimuthal and radial mode numbers =0) is exactly dmax. It is precomputed
        beforehand (either at the current chromaticity, or at Q'=0),
    - taub: bunch length (total, or 4*RMS for Gaussian bunches) in seconds,
    - dphase: additional phase of the damper in radian (w.r.t. to purely resistive damper when dphase=0)
    - M: number of bunches,
    - Nb: number of particles per bunch,
    - gamma: relativistic velocity factor,
    - Q: tune (with integer part included),
    - particle: 'proton' / 'electron' / 'Pb82'.
    '''


    e=1.60218e-19 # elementary charge in C

    # particle mass in kg, and number of elementary charges
    if particle.startswith('proton'):
        m0 = 1.6726e-27
        Z = 1
    elif particle.startswith('electron'):
        m0 = 9.1094e-31
        Z = 1
    elif particle.startswith('Pb54'):
        m0=(207.947/208.)*1.6726216e-27
    elif particle.startswith('Pb82'):
        # energy of Pb (isotope 208) at rest is 193.68715GeV (from J. Jowett)
        m0 = 193.68715*1e9*e/clight**2
        Z = 82
        
    else:
        raise ValueError("Pb with particle type")

    Ib=M*Nb*f0*Z*e # beam current
    beta=np.sqrt(1.-1./(gamma**2))

    bovertaub2=b/(taub*taub)
    coefdamper=(1j*f0*dmax*2.*bovertaub2/(g0*dnormfactor))*np.exp(1j*dphase)
    coefZ=1j*Ib*Z*e/(2.*gamma*m0*beta*clight*Q) # NM Feb 2019: added 1/beta for non-ultrarelativistic case

    return coefdamper,coefZ


def eigenmodesDELPHI(lmax,nmax,matdamper,matZ,coefdamper,coefZ,omegas,flageigenvect=False):

    ''' compute and diagonalize the final matrix (including impedance, damper and Qs)

    - lmax: number of azimuthal modes in matrices (we go from -lmax to +lmax)
    - nmax: number of radial modes in matrices, minus one (we go from 0 to +nmax)
    - matdamper: precomputed damper matrix,
    - matZ: precomputed imepdance matrix,
    - coefdamper: coefficient in front of damper matrix
    - coefZ: coefficient in front of impedance matrix
    - omegas: synchrotron angular frequency
    - flageigenvect: True to compute eigenvect (faster without)

    '''

    Mat=np.zeros((2*lmax+1,nmax+1,2*lmax+1,nmax+1),dtype=complex)

    for l in range(lmax,-lmax-1,-1):

        for lprime in range(lmax,-lmax-1,-1):

            for n in range(nmax+1):

                for nprime in range(nmax+1):

                    Mat[l+lmax,n,lprime+lmax,nprime] = coefdamper*matdamper[l+lmax,n,lprime+lmax,nprime]
                    Mat[l+lmax,n,lprime+lmax,nprime]+= coefZ*matZ[l+lmax,n,lprime+lmax,nprime]

                    if ( (l==lprime)and(n==nprime) ):
                        Mat[l+lmax,n,lprime+lmax,nprime] += omegas*l


    #t1=ti.time()

    if (flageigenvect):
        # compute eigenvectors
        eigenval,eigenvect=li.eig(Mat.reshape(((2*lmax+1)*(nmax+1),(2*lmax+1)*(nmax+1))))
    else:
        # do not compute eigenvectors (~ 2 times faster)
        eigenval=li.eigvals(Mat.reshape(((2*lmax+1)*(nmax+1),(2*lmax+1)*(nmax+1))))
        eigenvect=np.zeros(((2*lmax+1)*(nmax+1),(2*lmax+1)*(nmax+1)))

    #t2=ti.time()
    #print("lmax=",lmax,", nmax=",nmax,", time for eigenvalue pb resolution [seconds]: ",t2-t1)

    return eigenval,eigenvect.reshape((2*lmax+1,nmax+1,(2*lmax+1)*(nmax+1)))


def eigenmodesDELPHI_converged(
        nx, M, omegaksi, omega0, tunefrac, a, b, taub, g, Z, freqZ,
        coefdamper, coefZ, omegas, flag_trapz=0, flagdamperimp=0,
        d=None, freqd=None, kmax=5, crit=5.e-2, abseps=1.e-3,
        lmaxold=-1, nmaxold=-1, max_freq_old=0., matdamperold=None, matZold=None, 
        flageigenvect=False, lmin=1, nmin=1, optimized_convergence=True,
        max_allowed_n=np.inf, max_allowed_l=np.inf):

    """Computes eigenmodes for increasing matrix size, until we get convergence on
    the imaginary part of eigenvalues

    - nx: coupled-bunch mode considered (from 0 to M-1),
    - M: number of equidistant bunches,
    - omegaksi: chromatic angular frequency,
    - omega0: revolution angular frequency,
    - tunefrac: fractional part of the tune,
    - a,b: parameters for Laguerre polynomial decomposition in DELPHI,
    - taub: total bunch length (seconds) or 4*RMS for Gaussian bunches,
    - g: array with coef. of decomposition of initial distribution on Laguerre polynomials,
    - Z, freqZ: Z=damper impedance at the frequencies (NOT ANGULAR) freqZ
    - coefdamper, coefZ: coefficients in fromt resp. of damper and impedance matrix (precomputed beforehand),
    - omegas: synchrotron angular frequency.
    - flag_trapz: flag to use trapz method for computation of impedance matrix (if 1),
    - flagdamperimp: flag to use frequency dependent damper gain (if 1)(with d and freqd arrays),
    - d, freqd: d=damper impedance at the frequencies (NOT ANGULAR) freqd - used only if freqdamperimp==1,
    - kmax: number of eigenvalues to make converge,
    - crit: relative error tolerated on imaginary part of eigenvalues,
    - abseps: ~absolute error tolerated on imaginary part of eigenvalues (also used for absolute error determination
          on impedance and damper sums),
    - lmaxold, nmaxold: max azimuthal mode number of max radial mode number (radial mode begins at 0)
          already computed (in matdamperold and matZold),
    - max_freq_old: the maximum frequency until which the impedance sum was
          computed in matZold,
    - matdamperold ,matZold: damper and impedance matrices already computed,
    - lmin, nmin: minimum number of azimuthal and radial modes (lmin is actually
          the minimum lmax, and nmin the minimum nmax),
    - optimized_convergence: True to switch to a new convergence algorithm, where
          one increases first the number of radial modes until it converges,
          then the number of azimuthal modes, then of radial modes, etc., until
          full convergence is reached,
    - max_allowed_n and max_allowed_l: resp. the maximum possible nmax and lmax that are allowed.
    
    Returns the eigenvalues and eigenvectors (sorted from most unstable to least unstable)
    the lmax, nmax & max_freq of the matrices computed, and the damper and impedance matrices
    computed.
    """
    eigenvalold = np.ones(kmax)*(1.e50 + 1j*1.e50)
    err = 1e50
    lmax = lmin
    nmax = nmin

    # Absolute errors tolerated in damper and impedance sums computations
    absepsd = abseps/max(abs(coefdamper), 10)
    #print("absepsd=", absepsd)
    absepsZ = abseps/max(abs(coefZ.real), 10)
    #print("absepsZ=", absepsZ)

    flagcrit = True

    if not optimized_convergence:
        while flagcrit and (nmax<=max_allowed_n) and (lmax<=max_allowed_l):

            #t1 = ti.time()

            matdamper = compute_damper_matrix(
                                    lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
                                    a, b, taub, g, flagdamperimp=flagdamperimp,
                                    d=d, freqd=freqd, abseps=absepsd,
                                    lmaxold=lmaxold, nmaxold=nmaxold, damperold=matdamperold)

            matZ = compute_impedance_matrix_old(
                                    lmax, nmax, nx, M, omegaksi, omega0,
                                    tunefrac,a,b,taub,g,Z,freqZ, flag_trapz=flag_trapz,
                                    abseps=absepsZ, lmaxold=lmaxold, nmaxold=nmaxold, couplold=matZold)

            # t2 = ti.time()
            # print('lmax: {:d}, nmax: {:d}, time for computing matrices [seconds]: {:.2f}'.format(lmax, nmax, t2-t1))

            if (len(np.where(np.isinf(matdamper))[0]) == 0 and len(np.where(np.isnan(matdamper))[0]) == 0
                and (len(np.where(np.isinf(matZ))[0]) == 0 and len(np.where(np.isnan(matZ))[0]) == 0)):

                if (nmax+1)*(2*lmax+1)<=10000:
                    eigenval, v = eigenmodesDELPHI(lmax, nmax, matdamper, matZ,
                                                   coefdamper, coefZ, omegas,
                                                   flageigenvect=flageigenvect)
                else:
                    #print('Warning: eigenmodesDELPHI_converged: too big matrices, '
                    #       'convergence stopped, error={:.2E}, lmax={:d}, nmax={:d}'.format(err, lmax, nmax))
                    print("warning: eigenmodesDELPHI_converged: too big matrices, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))

            else:
                #print ('Warning: eigenmodesDELPHI_converged: some nan or inf in matrix, '
                #       'convergence stopped, error={:.2E}, lmax={:d}, nmax={:d}'.format(err, lmax, nmax))
                print("warning: eigenmodesDELPHI_converged: some nan or inf in matrix, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))
                break

            ind = np.argsort(np.imag(eigenval))

            kmaxtmp = min(kmax, len(eigenval))

            err = np.max(np.abs(np.imag(eigenval[ind[:kmaxtmp]])
                                -np.imag(eigenvalold[:kmaxtmp])) / (np.abs(np.imag(eigenvalold[:kmaxtmp]))
                                                                    + abseps))

            flagcrit = (err>crit) or (kmaxtmp<kmax)

            for k in range(min(kmax,len(eigenval))):
                eigenvalold[k] = eigenval[ind[k]]

            if lmax > lmaxold and nmax > nmaxold:
                lmaxold = lmax
                nmaxold = nmax
                matZold = matZ
                matdamperold = matdamper

            lmax += 1
            nmax += 1

    else:
        # new optimized algorithm, expanding radial modes as much as possible,
        # then azimuthal modes, then radial modes, etc., until convergence is reached
        didnotchecklmax = True
        didnotchecknmax = True
        if lmax<max_allowed_l:
            index_to_vary = 'l'
            index_before = lmax
        else:
            index_to_vary = 'n'
            index_before = nmax
        flagcrashed = False
        
        while flagcrit:

            #t1 = ti.time()
            # NM Feb. 2019: new way to check convergence: two successive loops expanding
            # radial modes, then azimuthal modes, etc. (rather than expanding both kinds
            # of modes at the same time)
            err = 1e50
            while (err>crit) and (nmax<=max_allowed_n) and (lmax<=max_allowed_l):
                
                #t1d = ti.time()
                matdamper = compute_damper_matrix(
                                        lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
                                        a, b, taub, g, flagdamperimp=flagdamperimp,
                                        d=d, freqd=freqd, abseps=absepsd,
                                        lmaxold=lmaxold, nmaxold=nmaxold, damperold=matdamperold)
                #t2d = ti.time()
                #t1i = ti.time()
                matZ,max_freq = compute_impedance_matrix(
                                        lmax, nmax, nx, M, omegaksi, omega0,
                                        tunefrac,a,b,taub,g,Z,freqZ, flag_trapz=flag_trapz,
                                        abseps=absepsZ, lmaxold=lmaxold, nmaxold=nmaxold,
                                        max_freq_old=max_freq_old, couplold=matZold)

                #t2i = ti.time()
                #print("lmax: {:d}, nmax: {:d}, max_freq: {:.2f} GHz, "
                #       "time for damper matrix [ms]: {:.2f}, time for imp. matrix [ms]: {:.2f}".format(
                #       lmax, nmax, max_freq/1e9, (t2d-t1d)*1e3, (t2i-t1i)*1e3))

                if (len(np.where(np.isinf(matdamper))[0]) == 0 and len(np.where(np.isnan(matdamper))[0]) == 0
                    and (len(np.where(np.isinf(matZ))[0]) == 0 and len(np.where(np.isnan(matZ))[0]) == 0)):

                    if (nmax+1)*(2*lmax+1)<=20000:
                        eigenval, v = eigenmodesDELPHI(lmax, nmax, matdamper, matZ,
                                                       coefdamper, coefZ, omegas,
                                                       flageigenvect=flageigenvect)
                    else:
                        #print('Warning: eigenmodesDELPHI_converged: too big matrices, '
                        #       'convergence stopped, error={:.2E}, lmax={:d}, nmax={:d}'.format(err, lmax, nmax))
                        print("warning: eigenmodesDELPHI_converged: too big matrices, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))
                        flagcrashed = True
                        break

                else:
                    #print ('Warning: eigenmodesDELPHI_converged: some nan or inf in matrix, '
                    #       'convergence stopped, error={:.2E}, lmax={:d}, nmax={:d}'.format(err, lmax, nmax))
                    print("warning: eigenmodesDELPHI_converged: some nan or inf in matrix, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))
                    flagcrashed = True
                    break

                ind = np.argsort(np.imag(eigenval))

                kmaxtmp = min(kmax, len(eigenval))

                err = np.max(np.abs(np.imag(eigenval[ind[:kmaxtmp]])
                                    -np.imag(eigenvalold[:kmaxtmp])) / (np.abs(np.imag(eigenvalold[:kmaxtmp]))
                                                                        + abseps))
                
                for k in range(kmaxtmp):
                    eigenvalold[k] = eigenval[ind[k]]

                lmaxold = lmax
                nmaxold = nmax
                max_freq_old = max_freq
                matZold = matZ
                matdamperold = matdamper

                if index_to_vary=='n':
                    didnotchecknmax = False
                    nmax += 1
                else:
                    didnotchecklmax = False
                    lmax += 1
            # end of the convergence on only one kind of mode
            
            if flagcrashed:
                break
            
            # stop only if the previous convergence loop stopped after one step 
            # (and if we have enough modes, and if both kinds of modes where checked at least once)
            if index_to_vary=='n':
                flagcrit =  (nmax>index_before+1) or (kmaxtmp<kmax) or didnotchecklmax
            else:
                flagcrit =  (lmax>index_before+1) or (kmaxtmp<kmax) or didnotchecknmax
            #print(flagcrit,index_to_vary,index_before,nmax,lmax,max_freq,didnotchecknmax,didnotchecklmax,err)
            # change the kind of mode to extend the matrix on
            if flagcrit:
                if index_to_vary=='n':
                    index_to_vary = 'l'
                    nmax -= 1
                    lmax += 1
                    index_before = lmax
                else:
                    index_to_vary = 'n'
                    lmax -= 1
                    nmax += 1
                    index_before = nmax

    return eigenval[ind], v[:,:,ind], lmaxold, nmaxold, max_freq_old, matdamperold, matZold


def diagonalmodesDELPHI_converged(
        nx, M, omegaksi, omega0, tunefrac, a, b, taub, g, Z, freqZ,
        coefdamper, coefZ, omegas, flag_trapz=0, flagdamperimp=0,
        d=None, freqd=None, kmax=5, crit=5.e-2, abseps=1.e-3,
        lmaxold=-1, nmaxold=-1, diagdamperold=None, diagZold=None):

    """Computes diagonal modes for increasing matrix size, until we get convergence on
    the imaginary part of the diagonal of the matrix

    - nx: coupled-bunch mode considered (from 0 to M-1),
    - M: number of equidistant bunches,
    - omegaksi: chromatic angular frequency,
    - omega0: revolution angular frequency,
    - tunefrac: fractional part of the tune,
    - a,b: parameters for Laguerre polynomial decomposition in DELPHI,
    - taub: total bunch length (seconds) or 4*RMS for Gaussian bunches,
    - g: array with coef. of decomposition of initial distribution on Laguerre polynomials,
    - Z, freqZ: Z=damper impedance at the frequencies (NOT ANGULAR) freqZ
    - coefdamper, coefZ: coefficients in fromt resp. of damper and impedance matrix (precomputed beforehand),
    - omegas: synchrotron angular frequency.
    - flag_trapz: flag to use trapz method for computation of impedance matrix (if 1),
    - flagdamperimp: flag to use frequency dependent damper gain (if 1)(with d and freqd arrays),
    - d, freqd: d=damper impedance at the frequencies (NOT ANGULAR) freqd - used only if freqdamperimp==1,
    - kmax: number of eigenvalues to make converge,
    - crit: relative error tolerated on imaginary part of eigenvalues,
    - abseps: ~absolute error tolerated on imaginary part of eigenvalues (also used for absolute error determination
          on impedance and damper sums),
    - lmaxold, nmaxold: max azimuthal mode number of max radial mode number (radial mode begins at 0)
          already computed (in diagdamperold and diagZold),
    - diagdamperold, diagZold: diagonal part of damper and impedance matrices already computed.
    
    Return the most unstable diagonal elements, and resp. the azimuthal and radial 
    mode number of the most unstable of them.
    """
    diagonalold = np.ones(kmax)*(1.e50 + 1j*1.e50)
    err = 1e50
    lmax = 20
    nmax = 20

    # Absolute errors tolerated in damper and impedance sums computations
    absepsd = abseps/max(abs(coefdamper), 10)
    #print("absepsd=", absepsd)
    absepsZ = abseps/max(abs(coefZ.real), 10)
    #print("absepsZ=", absepsZ)

    flagcrit = True

    while flagcrit:
        
        Matdiag = np.zeros((2*lmax+1,nmax+1),dtype=complex)
        
        diagZ = compute_impedance_diagonal(lmax, nmax, nx, M, omegaksi, omega0, tunefrac,
                                           a, b, taub, g, Z, freqZ, flag_trapz=0, abseps=1e-4,
                                           lmaxold=lmaxold, nmaxold=nmaxold, diagold=diagZold)

        diagdamper = compute_damper_diagonal(lmax,nmax,nx,M,omegaksi,omega0,tunefrac,
                                             a,b,taub,g,flagdamperimp=0,d=None,freqd=None,
                                             abseps=1e-4,lmaxold=lmaxold,nmaxold=nmaxold,
                                             damperdiagold=diagdamperold)

        for l in range(lmax,-lmax-1,-1):
            for n in range(nmax+1):
                Matdiag[l+lmax,n] = coefdamper*diagdamper[l+lmax,n]
                Matdiag[l+lmax,n]+= coefZ*diagZ[l+lmax,n]
                Matdiag[l+lmax,n]+= omegas*l
 
        diagonal = Matdiag.reshape(-1)
        if (len(np.where(np.isinf(diagonal))[0]) == 0 and len(np.where(np.isnan(diagonal))[0]) == 0):

            if (nmax+1)*(2*lmax+1)>10000:
                print("warning: diagonalmodesDELPHI_converged: too big matrices, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))

        else:
            print("warning: diagonalmodesDELPHI_converged: some nan or inf in diagonal, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))
            break

        ind = np.argsort(np.imag(diagonal))

        kmaxtmp = min(kmax, len(diagonal))

        err = np.max(np.abs(np.imag(diagonal[ind[:kmaxtmp]])
                            -np.imag(diagonalold[:kmaxtmp])) / (np.abs(np.imag(diagonalold[:kmaxtmp]))
                                                                + abseps))

        flagcrit = (err>crit) or (kmaxtmp<kmax)

        lmaxold = lmax
        nmaxold = nmax
        diagZold = diagZ
        diagdamperold = diagdamper

        for k in range(min(kmax,len(diagonal))):
            diagonalold[k] = diagonal[ind[k]]

        lmax += 1
        nmax += 1

    lmin, nmin = np.where(Matdiag==diagonal[ind[0]])
    lmin = abs(lmin[0]-lmax)
    nmin = nmin[0]
    
    return diagonal[ind[:kmaxtmp]], lmin, nmin


def eigenmodesDELPHI_converged_scan(
        Qpscan, nxscan, dampscan, Nbscan, omegasscan, dphasescan, M,
        omega0, Q, gamma, eta, a, b, taub, g, Z, freq,
        particle='proton', flagnorm=0, flag_trapz=0, flagdamperimp=0,
        d=None, freqd=None, kmax=1, kmaxplot=10, crit=5.e-2, abseps=1.e-3,
        flagm0=False, flageigenvect=False, flag_eta_dipole=False,
        flag_eta_crab=False, optimized_convergence=True,
        fixed_lmax_nmax=None):
    """Encapsulate DELPHI calculations, with scans on coupled-bunch modes,
    damper gain, number of particles, synchrotron tune and damper phase.
    Return a dict with tuneshifts (complex) for all these parameters scanned,
    the tuneshifts of the most unstable coupled-bunch modes, the most unstable
    coupled-bunch mode number (tuple (inx,nx) where nx=nxscan[inx]),
    and (optionally) the eigenvector corresponding to each tuneshift,
    the tuneshift from the mode 0, the sensitivity to noise (dipole noise and/or
    crab cavity noise).
    If fixed_lmax_nmax is a tuple (lmax,nmax), the computation is done
    at a fixed number of azimuthal and radial modes specified by these 
    numbers; instead if it's None then a convergence procedure is 
    performed on the number of modes, for each case of the scans.
    If flag_eta_dipole (resp. flag_eta_crab) is True, compute the sensitivity
    to dipole (resp. crab-cavity amplitude) noise - note that it requires also
    flageigenvect=True (see S. Furuseth and X. Buffat, Eur. Phys. J. Plus 137,
    506 (2022), https://doi.org/10.1140/epjp/s13360-022-02645-3) AND a Gaussian
    longitudinal distribution
    """
    f0 = omega0/(2.*np.pi)
    Qfrac = Q - np.floor(Q)
    
    if len(g)!=1 and (flag_eta_dipole or flag_eta_crab):
        raise NotImplementedError("cannot compute eta with non-Gaussian beam in longitudinal")

    if flagnorm == 0:
        # normalization factor for damper
        dnormfactor = compute_damper_matrix(
                                0, 0, 0, M, 0., omega0, Qfrac, a, b, taub, g, flagdamperimp=flagdamperimp,
                                d=d, freqd=freqd, abseps=abseps)
        dnormfactor = 2.*np.pi*dnormfactor[0,0,0,0]

    tuneshiftnx = np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),
                            len(omegasscan),len(dphasescan),kmaxplot), dtype=complex)
    tuneshift_most = np.zeros((len(Qpscan),len(dampscan),len(Nbscan),
                               len(omegasscan),len(dphasescan),kmaxplot), dtype=complex)
    nx_most = np.empty((len(Qpscan),len(dampscan),len(Nbscan),
                        len(omegasscan),len(dphasescan),kmaxplot), dtype=np.object_)

    if flageigenvect:
        eigenvectors = np.empty((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),
                                 len(omegasscan),len(dphasescan)), dtype=np.object_)
        eigenvectors_most = np.empty((len(Qpscan),len(dampscan),len(Nbscan),
                                      len(omegasscan),len(dphasescan),kmaxplot), dtype=np.object_)
        if flag_eta_dipole:
            eta_dipole = np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),
                            len(omegasscan),len(dphasescan),kmaxplot), dtype=float)
        if flag_eta_crab:
            eta_crab = np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),
                            len(omegasscan),len(dphasescan),kmaxplot), dtype=float)

    if flagm0:
        tuneshiftm0nx = np.zeros((len(Qpscan),len(nxscan),len(dampscan),
                                  len(Nbscan),len(omegasscan),len(dphasescan)), dtype=complex)
        tuneshiftm0 = np.zeros((len(Qpscan),len(dampscan),len(Nbscan),
                                len(omegasscan),len(dphasescan)), dtype=complex)

    for iQp,Qp in enumerate(Qpscan):
        omegaksi = Qp*omega0/eta
        for inx,nx in enumerate(nxscan):
            if flagnorm:
                # normalization factor for damper
                dnormfactor = compute_damper_matrix(
                                0, 0, nx, M, omegaksi, omega0, Qfrac, a, b, taub, g,
                                flagdamperimp=flagdamperimp, d=d, freqd=freqd, abseps=abseps)
                dnormfactor = 2.*np.pi*dnormfactor[0,0,0,0]

            # to keep track of maximum lmax and nmax for all modes
            lmaxi = -1
            nmaxi = -1
            lmax = -1
            nmax = -1
            max_freq = 0.
            matZ = None
            matdamper = None

            for idamp,damp in enumerate(dampscan):
                for iNb,Nb in enumerate(Nbscan):
                    for iomegas,omegas in enumerate(omegasscan):
                        for idphase,dphase in enumerate(dphasescan):

                            # print("Nb=",Nb,',lmax=',lmax,', nmax=',nmax)

                            coefdamper, coefZ = computes_coef(
                                                    f0, damp, b, g[0], dnormfactor, taub, dphase,
                                                    M, Nb, gamma, Q, particle=particle)

                            kwargs = {}
                            if fixed_lmax_nmax is not None:
                                lmin, nmin = fixed_lmax_nmax
                                kwargs.update({'max_allowed_l': lmin, 'max_allowed_n': nmin})
                            elif not optimized_convergence:
                                lmin, nmin = 1,1
                            else:
                                # first try to estimate the minimum number of modes
                                # to take into account, by exploring the imaginary
                                # part of the diagonal of the matrix
                                # THIS IS NOT SUFFICIENT
                                #freqshift_diag, lmin, nmin = diagonalmodesDELPHI_converged(
                                #                    nx, M, omegaksi, omega0, Qfrac, a, b, taub, g, Z,
                                #                    freq, coefdamper, coefZ, omegas, flag_trapz=flag_trapz,
                                #                    flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax,
                                #                    crit=crit, abseps=abseps, lmaxold=-1, nmaxold=-1,
                                #                    diagdamperold=None, diagZold=None)
                                #print("Qp=",Qp,", M=",M,", d=",damp,", Nb=",Nb,", omegas=",omegas,", dphase=",dphase)
                                #print("lmin=",lmin,", nmin=",nmin,", most unstable diagonal mode(s): ",freqshift_diag)
                                #lmin = max(lmin,1)
                                #nmin = max(nmin,1)
                                
                                # estimate the minimum size of the matrix rather from 
                                # the known position of the maximum of the Iln function
                                # (which is the same maximum as Gln when the bunch is Gaussian
                                # in longitudinal)
                                # max. of Iln is at sqrt(2*a*(2n+|l|))/taub
                                # (note: 2n+|l| is the number of nodes of one basis radial function)
                                number_of_nodes_at_maximum = int(round(((omegaksi+(1-Qfrac)*omega0)*taub)**2/(2*a)))
                                lmin = number_of_nodes_at_maximum//2+1
                                nmin = number_of_nodes_at_maximum//2+1
                                                                
                            (freqshift, v,
                             lmax, nmax, max_freq,
                             matdamper, matZ) = eigenmodesDELPHI_converged(
                                                    nx, M, omegaksi, omega0, Qfrac, a, b, taub, g, Z,
                                                    freq, coefdamper, coefZ, omegas, flag_trapz=flag_trapz,
                                                    flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax,
                                                    crit=crit, abseps=abseps, lmaxold=lmax, nmaxold=nmax, max_freq_old=max_freq,
                                                    matdamperold=matdamper, matZold=matZ, flageigenvect=flageigenvect,
                                                    lmin=lmin, nmin=nmin, optimized_convergence=optimized_convergence,
                                                    **kwargs)

                            if flagm0:
                                # extract mode m=0 tuneshift
                                tuneshiftm0nx[iQp,inx,idamp,iNb,
                                              iomegas,idphase] = extract_between_bounds(
                                                                        freqshift/omega0,
                                                                        -omegas/(2.*omega0), omegas/(2.*omega0))

                            if flageigenvect:
                                eigenvectors[iQp,inx,idamp,iNb,iomegas,idphase] = v
                                for k in range(min(kmaxplot,len(freqshift))):
                                    if flag_eta_dipole:
                                        eta_dipole[iQp,inx,idamp,iNb,iomegas,idphase,k],myNorm = \
                                            getDipoleNoiseSensitivity(v[:,:,k],omegaksi*taub/4.)
                                    if flag_eta_crab:
                                        eta_crab[iQp,inx,idamp,iNb,iomegas,idphase,k],_ = \
                                            getCrabAmplitudeNoiseSensitivity(v[:,:,k],omegaksi*taub/4.,
                                                myNorm=myNorm if flag_eta_dipole else None)

                            if len(freqshift) >= kmaxplot:
                                tuneshiftnx[iQp,inx,idamp,iNb,iomegas,idphase,:] = freqshift[:kmaxplot]/omega0

                            else:
                                tuneshiftnx[iQp,inx,idamp,iNb,iomegas,idphase,:len(freqshift)] = freqshift[:]/omega0

                            # lambdax[iNb,:] = freqshift[:kmaxplot]/omegas
                            lmaxi = max(lmax,lmaxi)
                            nmaxi = max(nmax,nmaxi)

        # find the most unstable coupled-bunch mode
        for idamp,damp in enumerate(dampscan):
            for iNb,Nb in enumerate(Nbscan):
                for iomegas,omegas in enumerate(omegasscan):
                    for idphase,dphase in enumerate(dphasescan):
                        for kmode in range(kmaxplot):

                            inx = np.argmin(np.imag(tuneshiftnx[iQp,:,idamp,iNb,iomegas,idphase,kmode]))
                            nx_most[iQp,idamp,iNb,iomegas,idphase,kmode] = (inx,nxscan[inx])

                            if kmode < kmax:
                                print("Qp= {} , M= {} , d= {} , Nb= {} , omegas= {} , dphase= {}".format(Qp,M,damp,Nb,omegas,dphase))
                                print("kmode= {} , Most unstable coupled-bunch mode:  {}".format(kmode,nxscan[inx]))
                            tuneshift_most[iQp,idamp,iNb,
                                           iomegas,idphase,kmode] = tuneshiftnx[iQp,inx,idamp,iNb,iomegas,idphase,kmode]
                            if flageigenvect:
                                try:
                                    eigenvectors_most[iQp,idamp,iNb,iomegas,idphase,
                                                      kmode] = eigenvectors[iQp,inx,idamp,iNb,iomegas,idphase][:,:,kmode]
                                except IndexError:
                                    # matrix size is not large enough - we leave the eigenvector empty
                                    pass

                        if flagm0 is True:
                            inx = np.argmax(np.abs(np.real(tuneshiftm0nx[iQp,:,idamp,iNb,iomegas,idphase])))

                            if kmode < kmax:
                                print("Qp= {} , M= {} , d= {} , Nb= {} , omegas= {} , dphase= {}".format(Qp,M,damp,Nb,omegas,dphase))
                                print("kmode= {} , coupled-bunch mode with highest tune shift:  {}".format(kmode,nxscan[inx]))
                            tuneshiftm0[iQp,idamp,iNb,
                                        iomegas,idphase] = tuneshiftm0nx[iQp,inx,idamp,iNb,iomegas,idphase]

    result_dict = {'tuneshiftnx': tuneshiftnx,
                   'tuneshift_most': tuneshift_most,
                   'nx_most': nx_most}

    if flagm0:
        result_dict['tuneshiftm0'] = tuneshiftm0

    if flageigenvect:
        result_dict['eigenvectors_most'] = eigenvectors_most
        if flag_eta_dipole:
            result_dict['eta_dipole'] = eta_dipole
        if flag_eta_crab:
            result_dict['eta_crab'] = eta_crab
        
    return result_dict


def eigenmodesDELPHI_converged_scan_lxplus(
        Qpscan, nxscan, dampscan, Nbscan, omegasscan, dphasescan, M,
        omega0, Q, gamma, eta, a, b, taub, g, Z, freq, particle='proton',
        flagnorm=0, flag_trapz=0, flagdamperimp=0, d=None, freqd=None,
        kmax=1, kmaxplot=10, crit=5.e-2, abseps=1.e-3, flagm0=False,
        flageigenvect=False, lxplusbatch=None, comment='',
        queue='1nh', dire='', **kwargs):
    """Same as eigenmodesDELPHI_converged_scan with possibility to launch
    on lxplus lxplusbatch:
        if None, no use of any queuing system
        if 'launch' -> launch calculation on lxplus (using HTCondor)
        if 'retrieve' -> retrieve outputs
    comment is used to identify the batch job name and the pickle filename
    dire is the directory where to put/find the result; it should be a path
    relative to the current directory (e.g. '../')
    """
    import pickle as pick
    
    result_dict = defaultdict(lambda : [])

    if lxplusbatch == None:
        result_dict = eigenmodesDELPHI_converged_scan(
                            Qpscan, nxscan, dampscan, Nbscan, omegasscan, dphasescan, M,
                            omega0, Q, gamma, eta, a, b, taub, g, Z, freq, particle=particle,
                            flagnorm=flagnorm, flag_trapz=flag_trapz,
                            flagdamperimp=flagdamperimp, d=d, freqd=freqd,
                            kmax=kmax, kmaxplot=kmaxplot, crit=crit, abseps=abseps,
                            flagm0=flagm0, flageigenvect=flageigenvect, **kwargs)

    else:
        root_filename = "DELPHI_pickle_"+comment
        input_filename = root_filename+'.npz'
        output_filename = 'out'+root_filename+'.npz'
        status, lsres = getstatusoutput('ls '+os.path.join(dire,output_filename))
        # print(status,lsres)

        # Launch jobs on HTCondor
        # Test first if output files are not already there
        if lxplusbatch.startswith('launch') and (status!=0):

            # Get the DELPHI script path
            for ii in range(0,len(sys.path)):
                if 'DELPHI_Python' in sys.path[ii]:
                    exec_path = sys.path[ii]

            # pickle file (now replaced by a numpy compressed file)
            param_dict = {'Qpscan': Qpscan, 'nxscan': nxscan, 'dampscan': dampscan,
                          'Nbscan': Nbscan, 'omegasscan': omegasscan,
                          'dphasescan': dphasescan,
                          'M': M, 'omega0': omega0, 'Q': Q, 'gamma': gamma,
                          'eta': eta, 'a': a, 'b': b, 'taub': taub, 'g': g,
                          'Z': Z, 'freq': freq, 'particle': particle,
                          'flagnorm': flagnorm, 'flag_trapz': flag_trapz,
                          'flagdamperimp': flagdamperimp, 'd': d, 'freqd': freqd,
                          'kmax': kmax, 'kmaxplot': kmaxplot, 'crit': crit,
                          'abseps': abseps, 'flageigenvect': flageigenvect,
                          'flagm0': flagm0,
                          }
            script_name = 'DELPHI_script.py'
            with open(os.path.join(exec_path,script_name),'r') as f:
                script_txt = f.read()

            _, python_exe = getstatusoutput("which python")
            exec_name = script_name.replace('.py','{}.py'.format(python_exe.replace('/','_')))
            print("Creating script {}".format(exec_name))
            with open(os.path.join(exec_path,exec_name),'w') as f:
                f.write("#!{}\n\n".format(python_exe))
                f.write(script_txt)
            
            param_dict.update(kwargs)
            np.savez_compressed(input_filename,**param_dict)

            # Select HTCondor JobFlavour equivalent to LSF queue
            if queue in ['8nm','1nh','8nh','1nd']:
                job_flavour = 'tomorrow'
            elif queue in ['2nd', '1nw','2nw']:
                job_flavour = 'nextweek'
            else:
                print('Error in LSF queue, cannot find HTCondor equivalent, use nextweek as default')
                job_flavour = 'nextweek'

            # Write the submission file and submit it to HTCondor
            # and write a script to move the data to the right folder
            filejob = open('batch'+comment+'.job','w')
            file_executable = open('move_output'+comment+'.sh', 'w')
            here = os.getcwd()
            file_executable.write("#!/bin/bash\n")
            file_executable.write("\n")
            file_executable.write("if [ -f "+output_filename+" ]; then\n")
            file_executable.write("  mv "+output_filename+" "+dire+"\n")
            file_executable.write("  mv "+os.path.join(here,input_filename)+" "+dire+"\n")
            file_executable.write("fi\n")
            file_executable.close()
            os.system('chmod 744 move_output'+comment+'.sh')

            filejob.write("executable = "+os.path.join(exec_path,exec_name)+"\n")
            filejob.write("arguments = "+input_filename+"\n")
            filejob.write("ID = $(Cluster).$(Process)\n")
            filejob.write("output = "+os.path.join(dire,"out_"+comment)+"\n")
            filejob.write("error = HTCondor_job_$(ID).err\n")
            filejob.write("log = HTCondor_job_$(Cluster).log\n")
            filejob.write("universe = vanilla\n")
            filejob.write("getenv = True\n")
            #filejob.write("environment = PYTHONPATH={}:\n".format(
            #                ":".join([path for path in sys.path])))
            filejob.write("should_transfer_files = YES\n")
            filejob.write("transfer_input_files = "+os.path.join(here,input_filename)+", move_output"+comment+".sh\n")
            filejob.write("when_to_transfer_output = ON_EXIT\n")
            filejob.write("+JobFlavour =\""+job_flavour+"\"\n")
            filejob.write("+PostCmd = \"move_output"+comment+".sh\"\n")
            filejob.write("queue\n")
            filejob.close()
            os.system("condor_submit batch"+comment+".job")

        # Retrieve the jobs from HTCondor
        else:
            status, lsres = getstatusoutput('ls '+os.path.join(dire,output_filename))
            # print(status, lsres)

            # Calculation went fine
            # Pickle the output files
            if status == 0:
                fileall = open(os.path.join(dire,output_filename), 'rb')
                result_dict = pick.load(fileall)
                fileall.close()

            # Problem during calculation - shows a warning and returns an empty
            # dictionary
            else:
                print("WARNING !!! no file {}".format(os.path.join(dire,output_filename)))

            os.system("rm -f "+input_filename+" batch"+comment+".job move_output"+comment+".sh")

    return result_dict


def DELPHI_wrapper(imp_mod,Mscan,Qpscan,dampscan,Nbscan,omegasscan,dphasescan,
        omega0,Qx,Qy,gamma,eta,a,b,taub,g,planes=['x','y'],nevery=1,freq_pywit_params={},
        particle='proton',flagnorm=0,flagdamperimp=0,d=None,freqd=None,
        kmax=1,kmaxplot=10,crit=5.e-2,abseps=1.e-3,flagm0=False,flageigenvect=False,
        lxplusbatch=None,comment='',queue='1nh',dire='',
        flagQpscan_outside=True,flag_trapz=None,**kwargs):

    '''Wrapper of eigenmodesDELPHI_converged_scan_lxplus, with more scans
    imp_mod is either an instance of the PyWIT "Model" class,
    or of the older IW2D class "impedance_wake" (in Impedance.py)
    from which one extracts the planes given in 'planes' (dipolar imp. only)
    Mscan is the scan in number of bunches
    lxplusbatch: if None, no use of lxplus batch system
                 if 'launch' -> launch calculation on lxplus on queue 'queue'
                 if 'retrieve' -> retrieve outputs
    comment is used to identify the batch job name and the pickle filename
    dire is the directory where to put/find the result; it should be a path
    relative to the current directory (e.g. '../')
    nevery indicates the downsampling (we take 1 frequency every "nevery"
    frequencies), in the case when imp_mod is an impedance_wake object.
    otherwise, freq_pywit_params is a dictionary of parameters to
    pass to the method "discretize" of the PyWIT "Component" class.
    flagQpscan_outside: True to put the Qpscan outside the lxplus batch job,
    False so that it is inside the lxplus batch job.
    '''

    from Impedance import test_impedance_wake_comp,impedance_wake

    strnorm=['','_norm_current_chroma']

    tuneshiftQp=np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),
                          len(Nbscan),len(omegasscan),len(dphasescan),kmaxplot),dtype=complex)
    nxQp=np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),
                          len(Nbscan),len(omegasscan),len(dphasescan),kmaxplot),dtype=np.object_)

    if flagm0:
        tuneshiftm0Qp=np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),
                                len(Nbscan),len(omegasscan),len(dphasescan)),dtype=complex)

    # To store the Evectors, we make space for the maximum number of
    # CB modes possbiles
    max_number_cb_modes = max(Mscan)
    if flageigenvect:
        #eigenvectors=np.empty((len(planes),max_number_cb_modes,len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan)),dtype=np.object_)
        eigenvectors=np.empty((len(planes),len(Mscan),len(Qpscan),len(dampscan),
                               len(Nbscan),len(omegasscan),len(dphasescan),kmaxplot),dtype=np.object_)
        if kwargs.get('flag_eta_dipole',False):
            eta_dipole=np.zeros((len(planes),len(Mscan),len(Qpscan),max_number_cb_modes,
                                 len(dampscan),len(Nbscan),len(omegasscan),
                                 len(dphasescan),kmaxplot),dtype=complex)
        if kwargs.get('flag_eta_crab',False):
            eta_crab=np.zeros((len(planes),len(Mscan),len(Qpscan),max_number_cb_modes,
                               len(dampscan),len(Nbscan),len(omegasscan),
                               len(dphasescan),kmaxplot),dtype=complex)

            
    for iplane,plane in enumerate(planes):

        # select Zxdip or Zydip
        flagx=int(plane=='x')
        try:
            # try with a pywit imp. model
            comp = 'x1000' if flagx else 'y0100'
            Ztmp = imp_mod.total.get_component(comp).discretize(**freq_pywit_params)
            freq = Ztmp[0][0]
            Z = np.hstack((np.real(Ztmp[0][1]).reshape((-1,1)),
                           np.imag(Ztmp[0][1]).reshape((-1,1))))
        except AttributeError:
            # fall back to an impedance_wake object from IW2D
            for iw in imp_mod:
                if test_impedance_wake_comp(iw,flagx,1-flagx,0,0,plane):
                    Z=deepcopy(iw.func[::nevery,:])
                    freq=deepcopy(iw.var[::nevery])

        for iM,M in enumerate(Mscan):

            if flag_trapz is None:
                flag_trapz_forM = 1 if M==1 else 0 # by default no trapz method except in single-bunch
            else:
                flag_trapz_forM = flag_trapz

            if (M==1):
                nxscan=np.array([0])
            #elif (M==1782):
            #   nxscan=np.array([0, 1, 300, 600, 880, 890, 891, 892, 900, 910, 950, 1000, 1200, 1500, 1780, 1781])
            #elif (M==3564):
            #   nxscan=np.array([0, 1, 300, 600, 900, 1200, 1500, 1770, 1780, 1781, 1782, 1785, 1790, 1800, 1900, 2000, 2300, 2600, 2900, 3200, 3500, 3560, 3561, 3562, 3563])
            else:
                # nxscan=sort_and_delete_duplicates(np.concatenate((np.arange(0,M,M/20),np.arange(M/2-10,M/2+11),np.arange(M-10,M),np.arange(0,10))))
                # print("number of coupled-bunch modes=",len(nxscan))
                nxscan = np.arange(0,M)
                print("number of coupled-bunch modes={}".format(len(nxscan)))

            if flagQpscan_outside:
                # put loop on Qpscan outside the lxplus job
                for iQp,Qp in enumerate(Qpscan):
                    #tuneshiftnx=np.zeros((1,len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),kmaxplot),dtype=complex)
                    #if flageigenvect==True:
                    #    eigenvectors=np.empty((1,len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan)),dtype=np.object_)

                    result_dict = eigenmodesDELPHI_converged_scan_lxplus([Qp],
                        nxscan,dampscan,Nbscan,omegasscan,dphasescan,M,omega0,eval('Q'+plane),gamma,eta,
                        a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                        flag_trapz=flag_trapz_forM,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                        kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,flageigenvect=flageigenvect,
                        lxplusbatch=lxplusbatch,comment=comment+'_'+str(M)+'b_Qp'+str(Qp)+strnorm[flagnorm]+'_'+plane,
                        queue=queue,dire=dire,**kwargs)
                    
                    if len(result_dict['tuneshift_most'])!=0:
                        #tuneshiftnx = result_dict['tuneshiftnx']
                        tuneshiftQp[iplane,iM,iQp,:,:,:,:,:] = result_dict['tuneshift_most']
                        nxQp[iplane,iM,iQp,:,:,:,:,:] = result_dict['nx_most']

                        if flagm0:
                            tuneshiftm0Qp[iplane,iM,iQp,:,:,:,:] = result_dict['tuneshiftm0']

                        if flageigenvect:
                            #eigenvectors[iplane,ix_storage_EVectors:(ix_storage_EVectors+len(nxscan)),iQp,:,:,:,:] = result_dict['eigenvectors']
                            eigenvectors[iplane,iM,iQp,:,:,:,:,:] = result_dict['eigenvectors_most']
                            if kwargs.get('flag_eta_dipole',False):
                                eta_dipole[iplane,iM,iQp,:M,:,:,:,:,:] = result_dict['eta_dipole']
                            if kwargs.get('flag_eta_crab',False):
                                eta_crab[iplane,iM,iQp,:M,:,:,:,:,:] = result_dict['eta_crab']

            else:
                # put loop on Qpscan inside the lxplus job
                #tuneshiftnx=np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),kmaxplot),dtype=complex)
                result_dict = eigenmodesDELPHI_converged_scan_lxplus(Qpscan,
                        nxscan,dampscan,Nbscan,omegasscan,dphasescan,M,omega0,eval('Q'+plane),gamma,eta,
                        a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                        flag_trapz=flag_trapz_forM,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                        kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,flageigenvect=flageigenvect,
                        lxplusbatch=lxplusbatch,comment=comment+'_'+str(M)+'b'+strnorm[flagnorm]+'_'+plane,
                        queue=queue,dire=dire,**kwargs)

                if len(result_dict['tuneshift_most'])!=0:
                    #tuneshiftnx = result_dict['tuneshiftnx']
                    tuneshiftQp[iplane,iM,:,:,:,:,:,:] = result_dict['tuneshift_most']
                    nxQp[iplane,iM,:,:,:,:,:,:] = result_dict['nx_most']

                    if flagm0:
                        tuneshiftm0Qp[iplane,iM,:,:,:,:,:] = result_dict['tuneshiftm0']

                    if flageigenvect:
                        #eigenvectors[iplane,ix_storage_EVectors:(ix_storage_EVectors+len(nxscan)),iQp,:,:,:,:] = result_dict['eigenvectors']
                        eigenvectors[iplane,iM,:,:,:,:,:,:] = result_dict['eigenvectors_most']
                        if kwargs.get('flag_eta_dipole',False):
                            eta_dipole[iplane,iM,:,:,:,:,:,:,:] = result_dict['eta_dipole']
                        if kwargs.get('flag_eta_crab',False):
                            eta_crab[iplane,iM,:,:,:,:,:,:,:] = result_dict['eta_crab']

    result_dict = {'tuneshiftQp': tuneshiftQp,
                   'nxQp': nxQp}
    if flagm0:
        result_dict['tuneshiftm0Qp'] = tuneshiftm0Qp

    if flageigenvect:
        result_dict['eigenvectorsQp'] = eigenvectors
        if kwargs.get('flag_eta_dipole',False):
            result_dict['eta_dipole'] = eta_dipole
        if kwargs.get('flag_eta_crab',False):
            result_dict['eta_crab'] = eta_crab

    return result_dict


def dispersion_integral_oct_2D(Q,bx,bxy,distribution='gaussian'):

    ''' computes the dispersion integral in 2D from an octupole with detuning
    per sigma given by bx (in the plane of the coherent motion) and bxy (in the
    other plane). Compute the integral for a certain coherent complex tune shift Q.
    The transverse distribution can be 'gaussian' or 'parabolic'.
    This is the integral vs Jx and Jy of Jx*dphi/dJx/(Q-bx*Jx-bxy*Jy-i0) (with phi the distribution function)

    NOTE: for stability diagrams, use -1/dispersion_integral, and usually the convention is to plot
    -Im[Q] vs Re[Q] (beware of all these minus signs).
    '''

    from scipy.special import exp1

    if (np.imag(Q)==0): Q=Q-1e-15*1j
    c=bxy/bx
    q=-Q/bx


    if (distribution=='gaussian'):

        I1=(1-c-(q+c-c*q)*np.exp(q)* exp1(q) + c*np.exp(q/c)* exp1(q/c)) / ((1-c)**2)

        if np.isnan(I1): I1=1./q-(c+2)/q**2 # asymptotic form for large q (assuming c is of order 1)

    elif (distribution=='parabolic'):

        xi=q/5.

        #I1=((c+xi)**3 *np.log(1+xi)-(c+xi)**3.*np.log(c+xi)+(-1+c)*(c*(c+2*c*xi+(-1+2*c)*xi**2) +
        #       (-1+c)*xi**2 *(3*c+xi+2*c*xi)*(np.log(xi)-np.log(1+xi))))/((-1+c)**2*c**2)
        I1=((c+xi)**3 *np.log((1+xi)/(c+xi))+(-1+c)*(c*(c+2*c*xi+(-1+2*c)*xi**2) +
                (-1+c)*xi**2 *(3*c+xi+2*c*xi)*np.log(xi/(1+xi))))/((-1+c)**2*c**2)
        I1=-I1*4./5.
        # I checked this is the same as in Scott Berg-Ruggiero CERN SL-AP-96-71 (AP)

        if (np.abs(xi)>100.): I1=1./q-(c+2)/q**2 # asymptotic form for large q (assuming c is of order 1) (obtained thanks to Mathematica - actually the same as for Gaussian)

    I=-I1/bx
    I=-I # additional minus sign because for DELPHI we want the integral with
    # dphi/dJx (derivative of distribution) on the numerator, so -[the one of Berg-Ruggiero]

    return I


def detuning_coef_oct_LHC(plane,gamma,epsnormx,epsnormy,current_foc_oct,current_defoc_oct):

    ''' compute detuning coefficients (per transverse sigma) bx (in the plane of the coherent motion)
    and bxy (in the other plane) for LHC octupoles with standard (i.e. non ATS) optics
    (squeeze does not matter, but ATS does).
    Input parameters:
    - plane: plane of coherent motion: 'x' if horizontal, 'y' if vertical
     (sligthly different coefficients).
    - gamma: relativistic mass factor of the beam,
    - epsnormx: normalized emittance in the plane (x or y) studied (e.g. 3.75e-6 m at 7TeV),
    - epsnormy: normalized emittance in the plane (x or y) perpendicular to the plane studied (e.g. 3.75e-6 m at 7TeV),
    - current_foc_oct: current in the focusing octupoles (max is supposed to be 550A),
    - current_defoc_oct: current in the defocusing octupoles (max is supposed to be 550A),
    '''

    current_max=550.

    beta=np.sqrt(1.-1./gamma**2) # relativistic velocity factor
    # reduction factor for the octupole current and the energy
    F=(current_foc_oct/current_max)*(7460.52/gamma)
    D=(current_defoc_oct/current_max)*(7460.52/gamma)

    eps1sigmax = epsnormx/(beta*gamma)
    eps1sigmay = epsnormy/(beta*gamma)

    # use madx value of O_3 (63100 T/m3) and madx beta functions (checked with S. Fartoukh).
    # We put the same values as in HEADTAIL.
    if plane=='y':
        # vertical
        ax=9789*F-277203*D
    elif plane=='x':
        # horizontal
        ax=267065*F-7856*D

    axy=-102261*F+93331*D

    bx=ax*eps1sigmax
    bxy=axy*eps1sigmay

    return bx,bxy


def determinantDELPHI_tunespread(dQc,lmax,nmax,matdamper,matZ,coefdamper,coefZ,bx,bxy,omega0,omegas,distribution='gaussian'):

    ''' compute the determinant of the final matrix (including impedance, damper and transverse Landau damping)
    as a function of coherent tune shift dQc. This function has to be solved (vs. dQc) to find
    the eigenmodes.

    - dQc: coherent tune shift (from unperturbed tune Q0).
    - lmax: number of azimuthal modes in matrices (we go from -lmax to +lmax)
    - nmax: number of radial modes in matrices, minus one (we go from 0 to +nmax)
    - matdamper: precomputed damper matrix,
    - matZ: precomputed impedance matrix,
    - coefdamper: coefficient in front of damper matrix
    - coefZ: coefficient in front of impedance matrix
    - bx & bxy: detuning coefficients for the transverse tunespread (in units of sigma),
      in the plane of the coherent motion (bx) and in the other plane (bxy)
    - omega0: revolution angular frequency
    - omegas: synchrotron angular frequency
    - distribution: kind of transverse distribution ('gaussian' or 'parabolic')
    '''

    Mat=np.zeros((2*lmax+1,nmax+1,2*lmax+1,nmax+1),dtype=complex)

    for l in range(lmax,-lmax-1,-1):

        for lprime in range(lmax,-lmax-1,-1):

            for n in range(nmax+1):

                for nprime in range(nmax+1):

                    Mat[l+lmax,n,lprime+lmax,nprime] = coefdamper*matdamper[l+lmax,n,lprime+lmax,nprime]
                    Mat[l+lmax,n,lprime+lmax,nprime]+= coefZ*matZ[l+lmax,n,lprime+lmax,nprime]

                    if ( (l==lprime)and(n==nprime) ):

                        # tune offset from azimuthal mode number (synchrotron sideband)
                        Qls=omegas*l/omega0
                        # dispersion integral (for transverse Landau damping)
                        if np.abs(bx*bxy)>0.:
                            I=dispersion_integral_oct_2D(dQc-Qls,bx,bxy,distribution=distribution)
                            # final matrix with Landau damping included
                            Mat[l+lmax,n,lprime+lmax,nprime] += omega0/I
                        else:
                            # final matrix without Landau damping
                            Mat[l+lmax,n,lprime+lmax,nprime] += omega0*(Qls-dQc)

    det=li.det(Mat.reshape(((2*lmax+1)*(nmax+1),(2*lmax+1)*(nmax+1))))
    #print("dQc=",dQc,", det=",det)

    return det


def determinantDELPHI_tunespread_array(dQc,lmax,nmax,matdamper,matZ,coefdamper,coefZ,bx,bxy,omega0,omegas,distribution='gaussian'):

    ''' compute the determinant of the final matrix (including impedance, damper and transverse Landau damping)
    as a function of coherent tune shift dQc. Version where dQc can be an array.

    - dQc: coherent tune shift (from unperturbed tune Q0). Can be a scalar or an array.
    - lmax: number of azimuthal modes in matrices (we go from -lmax to +lmax)
    - nmax: number of radial modes in matrices, minus one (we go from 0 to +nmax)
    - matdamper: precomputed damper matrix,
    - matZ: precomputed impedance matrix,
    - coefdamper: coefficient in front of damper matrix
    - coefZ: coefficient in front of impedance matrix
    - bx & bxy: detuning coefficients for the transverse tunespread (in units of sigma),
      in the plane of the coherent motion (bx) and in the other plane (bxy)
    - omega0: revolution angular frequency
    - omegas: synchrotron angular frequency
    - distribution: kind of transverse distribution ('gaussian' or 'parabolic')
    '''

    dQc_list=create_list(dQc,n=1)
    det=np.zeros(len(dQc_list),dtype=complex)

    for idQc,dQc in enumerate(dQc_list):

        det[idQc]=determinantDELPHI_tunespread(dQc,lmax,nmax,matdamper,matZ,coefdamper,coefZ,bx,bxy,omega0,omegas,distribution='gaussian')

    return det


def solve_determinantDELPHI_tunespread(lmax,nmax,matdamper,matZ,coefdamper,coefZ,bx,bxy,
        omega0,omegas,distribution='gaussian',kini=12):#,minimag=None,maximag=None,npts_all=None,npts_zoom=100,kini=10):

    ''' solve the equation determinant(final matrix)=0 (matrix includes impedance, damper and transverse Landau damping)
    as a function of coherent tune shift, to find the eigenmodes.

    - lmax: number of azimuthal modes in matrices (we go from -lmax to +lmax)
    - nmax: number of radial modes in matrices, minus one (we go from 0 to +nmax)
    - matdamper: precomputed damper matrix,
    - matZ: precomputed impedance matrix,
    - coefdamper: coefficient in front of damper matrix,
    - coefZ: coefficient in front of impedance matrix,
    - bx & bxy: detuning coefficients for the transverse tunespread (in units of sigma),
      in the plane of the coherent motion (bx) and in the other plane (bxy),
    - omega0: revolution angular frequency,
    - omegas: synchrotron angular frequency,
    - distribution: kind of transverse distribution ('gaussian' or 'parabolic'),
    - kini: number of eigenvalues of the linear system (without Landau damping)
    to initialize the solving algorithm.
    '''
    from solve_lib import potential_roots_complex,roots_complex_list

    #if minimag==None: minimag=-omegas/omega0
    #if maximag==None: maximag=0
    #if npts_all==None: npts_all=2*lmax+1

    minimag = -omegas/omega0
    maximag = 0
    npts_all = 2*lmax+1

    # f=function to solve is the matrix determinant (normalized to the value for zero coherent tuneshift)
    factnorm = np.abs(determinantDELPHI_tunespread(0,lmax,nmax,matdamper,matZ,
                                                    coefdamper,coefZ,bx,bxy,omega0,
                                                    omegas,distribution=distribution))
    f=(lambda dQc: determinantDELPHI_tunespread(dQc,lmax,nmax,matdamper,matZ,
                                                coefdamper,coefZ,bx,bxy,omega0,
                                                omegas,distribution=distribution)/factnorm)

    # first solve the eigenvalue problem without Landau damping (provide initial guesses)
    freqshift,v = eigenmodesDELPHI(lmax,nmax,matdamper,matZ,coefdamper,coefZ,omegas,flageigenvect=False)
    ind = np.argsort(np.imag(freqshift))
    # kini cannot be more than the number of eigenvalues with negative imag. part
    kini = max(kini,len(np.where(np.imag(freqshift[ind])<0)))

    list_pot_roots=[]

#    # OLD pre-solving algorithm (find all potential roots) (perform very poorly)
#    list_pot_roots=potential_roots_complex(f,-lmax*(1.+1./(npts_all-1.))*omegas/omega0,lmax*(1.+1./(npts_all-1.))*omegas/omega0,minimag,maximag,
#       npts=npts_all,meshkind='lin')
#    print("pre-solving (global) -> done",len(list_pot_roots))
#
#    # 1st version: zoom around synchrotron sidebands
#    for l in range(-lmax,lmax+1):
#
#       #list_tmp=potential_roots_complex(f,(l-0.5)*omegas/omega0,(l+0.5)*omegas/omega0,minimag,maximag,
#       #       npts=npts_zoom,meshkind='log',offset_log=l*omegas/omega0+1j*(minimag+maximag)/2.)
#       list_tmp=potential_roots_complex(f,(l-0.5)*omegas/omega0,(l+0.5)*omegas/omega0,minimag,maximag,
#               npts=npts_zoom,meshkind='lin',offset_log=l*omegas/omega0)
#
#       print("pre-solving (l=",l,") -> done",len(list_tmp),(l-0.5)*omegas/omega0,(l+0.5)*omegas/omega0,minimag,maximag)
#
#       for el in list_tmp:
#           if not(el in list_pot_roots): list_pot_roots.append(el)
#
#    # 2nd version: zoom around eigenvalues of unperturbed system
#    for i in ind[:kini]:
#
#       minimag=np.imag(freqshift[i])/omega0
#       minreal=min(np.real(freqshift[i])/omega0,np.real(freqshift[i])/omega0+2*(2*bx+bxy))
#       maxreal=max(np.real(freqshift[i])/omega0,np.real(freqshift[i])/omega0+2*(2*bx+bxy))
#       list_tmp=potential_roots_complex(f,minreal,maxreal,minimag,maximag,
#               npts=npts_all,meshkind='lin',offset_log=np.real(freqshift[i])/omega0)
#
#       print("pre-solving (i=",i,") -> done",len(list_tmp),2*(2*bx+bxy),minreal,maxreal,minimag,maximag)
#
#       for el in list_tmp:
#           if not(el in list_pot_roots): list_pot_roots.append(el)
#
    # NEW algorithm, much simpler (and works): take unperturbed eigenvalues
    # as initial guesses, plus the same with a real offset from the tunespread
    print("pre-solving for {} initial eigenvalues w/o tunespread".format(len(ind[:kini])))
    for fr in freqshift[ind[:kini]]:
        list_pot_roots.append((np.real(fr)/omega0,np.imag(fr)/omega0))
        
        if (2*abs(2*bx+bxy)>1e-7):
            list_pot_roots.append((np.real(fr)/omega0+2*(2*bx+bxy),np.imag(fr)/omega0))

        # then zoom around eigenvalues of unperturbed system
        minimag = np.imag(fr)/omega0
        maximag = 0.
        minreal = min(np.real(fr)/omega0,np.real(fr)/omega0+2*(2*bx+bxy))
        maxreal = max(np.real(fr)/omega0,np.real(fr)/omega0+2*(2*bx+bxy))
        list_tmp = potential_roots_complex(f,minreal,maxreal,minimag,maximag,
                                            npts=20,meshkind='lin',offset_log=np.real(fr)/omega0)

        #print("pre-solving (unperturbed dQ=",fr/omega0,") -> done",len(list_tmp))#,list_tmp

        for el in list_tmp:
            if not(el in list_pot_roots):
                list_pot_roots.append(el)

    print("found {} potential roots".format(len(list_pot_roots)))
    #print(list_pot_roots)
    #arr_pot_roots=np.array(list_pot_roots)*omega0

    # actual solving algorithm (note: roots translated from tune shifts to angular frequency shits)
    eigenval = omega0*roots_complex_list(f,list_pot_roots,tolf=1e-3,tolx=1e-10)
    print("found {} roots".format(len(eigenval)))

    return eigenval#,arr_pot_roots


def solve_stability_diagram(lmax,nmax,matdamper,matZ,coefdamper,coefZ,bx,bxy,
        omega0,omegas,distribution='gaussian',kini=12):#,minimag=None,maximag=None,npts_all=None,npts_zoom=100,kini=10):

    ''' solve the stability diagram equation dQu*dispersion_integral(Qc)=-1
    with dQu the unperturbed coherent tune shifts from DELPHI (using impedance & damper
    but not Landau damping) as a function of coherent tune shift Qc, to
    find the eigenmodes with transverse Landau damping (stability diagram approx.).

    - lmax: number of azimuthal modes in matrices (we go from -lmax to +lmax)
    - nmax: number of radial modes in matrices, minus one (we go from 0 to +nmax)
    - matdamper: precomputed damper matrix,
    - matZ: precomputed impedance matrix,
    - coefdamper: coefficient in front of damper matrix,
    - coefZ: coefficient in front of impedance matrix,
    - bx & bxy: detuning coefficients for the transverse tunespread (in units of sigma),
      in the plane of the coherent motion (bx) and in the other plane (bxy),
    - omega0: revolution angular frequency,
    - omegas: synchrotron angular frequency,
    - distribution: kind of transverse distribution ('gaussian' or 'parabolic'),
    - kini: number of eigenvalues of the linear system (without Landau damping)
    to initialize the solving algorithm.

    NOTE: BUGGY - use rather the function 'solve_stability_diagram_single_tuneshift'
    '''

    from solve_lib import potential_roots_complex,roots_complex_list

    # first solve the eigenvalue problem without Landau damping (provide initial
    # guesses & unperturbed coherent tune shifts)
    freqshift,v=eigenmodesDELPHI(lmax,nmax,matdamper,matZ,coefdamper,coefZ,omegas,flageigenvect=False)
    ind=np.argsort(np.imag(freqshift))
    # kini cannot be more than the number of eigenvalues with negative imag. part
    kini=max(kini,len(np.where(np.imag(freqshift[ind])<0)))

    eigenval=[]

    # solve for each unperturbed eigenvalue
    for fr in freqshift[ind[:kini]]:

        # evaluate azimuthal mode number (used for stability diagram)
        l=np.int(np.ceil(fr.real/omegas))
        #print("fr=",fr/omega0,", l=",l)

        # solve equation
        Qls=omegas*l/omega0
        # f: function to solve is the stability diagram eq. : dQu*dispersion_integral(Qc)=-1
        f=(lambda dQc: (fr/omega0)*dispersion_integral_oct_2D(dQc-Qls,bx,bxy,distribution=distribution)+1)

        # take unperturbed eigenvalues as initial guesses, plus the same with a
        # real offset from the tunespread
        list_pot_roots=[(np.real(fr)/omega0-Qls,np.imag(fr)/omega0)]
        if (2*abs(2*bx+bxy)>1e-7): list_pot_roots.append((np.real(fr)/omega0+2*(2*bx+bxy),np.imag(fr)/omega0))

        #print(list_pot_roots)

        # actual solving algorithm (note: roots translated from tune shifts to angular frequency shits)
        eigenvaltmp=omega0*roots_complex_list(f,list_pot_roots,tolf=1e-3,tolx=1e-10)
        #print(eigenvaltmp)
        if len(eigenvaltmp)>1:
            print("Pb in solve_stability_diagram: too many solutions {} {}".format(len(eigenvaltmp),eigenvaltmp))

        if len(eigenvaltmp)>=1:
            itmp=np.argmin(np.imag(eigenvaltmp))
            eigenval.append(eigenvaltmp[itmp]+Qls*omega0)# keep here most critical one (arbitrary choice...)
        else:
            print("Pb in solve_stability_diagram: did not find any solution {} {} {}".format(fr/omega0,l,len(eigenvaltmp)))

    return np.array(eigenval)


def solve_stability_diagram_single_tuneshift(dQu,bx,bxy,Qs,distribution='gaussian'):

    ''' solve the stability diagram equation dQu*dispersion_integral(Qc)=-1
    with dQu the unperturbed coherent tune shifts (from e.g. DELPHI w/o tunespread)
    as a function of coherent tune shift Qc, to
    find the eigenmodes with transverse Landau damping (stability diagram approx.).

    - dQu: unperturbed coherent tuneshift (obtained w/o tunespread),
    - bx & bxy: detuning coefficients for the transverse tunespread (in units of sigma),
      in the plane of the coherent motion (bx) and in the other plane (bxy),
    - Qs: synchrotron tune,
    - distribution: kind of transverse distribution ('gaussian' or 'parabolic'),
    '''

    from solve_lib import potential_roots_complex,roots_complex_list

    # evaluate azimuthal mode number (used for stability diagram)
    l=np.int(np.ceil(np.real(dQu)/Qs))
    #print("dQu=",dQu,", l=",l)

    # take away the shift from azimuthal mode number
    Qls=Qs*l
    dQu -= Qls

    # first evaluate the stability diagram
    stab = [-1./dispersion_integral_oct_2D(Q,bx,bxy,distribution='gaussian')
                 for e in (-1,1) for Q in bx*e*10.**np.arange(-3,2,0.01)[::e]]

    # case when we are under the stability diagram
    # (beware of the sign: -imag(dQu) needs to be under -imag(stab. diagram))
    if np.imag(dQu)>np.interp(np.real(dQu),np.real(stab),np.imag(stab)):
        #print("Stable case")
        return 0.

    # f: function to solve -  corresponds to equation : dQu*dispersion_integral(Qc)=-1
    f=(lambda dQc: dQu*dispersion_integral_oct_2D(dQc,bx,bxy,distribution=distribution)+1)

    # take unperturbed tuneshift as initial guess, plus the same with a
    # real offset from the tunespread, plus some guess from the stability diagram
    # (imag. part - value of the stab. diag. at the real part of the tuneshift dQu)
    list_pot_roots=[(np.real(dQu),np.imag(dQu))]
    if (2*abs(2*bx+bxy)>1e-7):
        list_pot_roots.append((np.real(dQu)+2*(2*bx+bxy),np.imag(dQu)))
    list_pot_roots.append((np.real(dQu),np.imag(dQu)-np.interp(np.real(dQu),np.real(stab),np.imag(stab))))

    #print(list_pot_roots)

    # actual solving algorithm (note: roots translated from tune shifts to angular frequency shits)
    dQtmp=roots_complex_list(f,list_pot_roots,tolf=1e-3,tolx=1e-10)
    #print(dQtmp)
    if len(dQtmp)>1:
        print("Pb in solve_stability_diagram: too many solutions {} {}".format(len(dQtmp),dQtmp))
        return None

    if len(dQtmp)==1:
        dQ = dQtmp[0]+Qls
        if np.abs(f(dQ-Qls))>1e-8:
            print("Pb in solve_stability_diagram: no proper solution found (dQ={}, function value: {})".format(dQ,f(dQ)))
            return np.nan
        else:
            return dQ

    else:
        print("Pb in solve_stability_diagram: did not find any solution for {} (l={})".format(dQu,l))
        return None


def eigenmodesDELPHI_tunespread_converged(nx,M,omegaksi,omega0,tunefrac,a,b,taub,g,Z,freqZ,coefdamper,coefZ,
        omegas,bx,bxy,flag_trapz=0,flagdamperimp=0,d=None,freqd=None,kmax=5,crit=5.e-2,abseps=1.e-3,
        lmaxold=-1,nmaxold=-1,matdamperold=None,matZold=None,distribution='gaussian',kini=12):#,minimag=None,maximag=None,npts_all=1000,npts_zoom=1000):

    ''' computes eigenmodes for increasing matrix size, until we get convergence on
    the imaginary part of eigenvalues. We include tunespread.

    - nx: coupled-bunch mode considered (from 0 to M-1),
    - M: number of equidistant bunches,
    - omegaksi: chromatic angular frequency,
    - omega0: revolution angular frequency,
    - tunefrac: fractional part of the tune,
    - a,b: parameters for Laguerre polynomial decomposition in DELPHI,
    - taub: total bunch length (seconds) or 4*RMS for Gaussian bunches,
    - g: array with coef. of decomposition of initial distribution on Laguerre polynomials,
    - Z, freqZ: Z=damper impedance at the frequencies (NOT ANGULAR) freqZ
    - coefdamper, coefZ: coefficients in fromt resp. of damper and impedance matrix (precomputed beforehand),
    - omegas: synchrotron angular frequency.
    - bx & bxy: detuning coefficients for the transverse tunespread (in units of sigma),
      in the plane of the coherent motion (bx) and in the other plane (bxy)
    - flag_trapz: flag to use trapz method for computation of impedance matrix (if 1),
    - flagdamperimp: flag to use frequency dependent damper gain (if 1)(with d and freqd arrays),
    - d, freqd: d=damper impedance at the frequencies (NOT ANGULAR) freqd - used only if freqdamperimp==1,
    - kmax: number of eigenvalues to make converge,
    - crit: relative error tolerated on imaginary part of eigenvalues,
    - abseps: ~absolute error tolerated on imaginary part of eigenvalues (also used for absolute error determination
          on impedance and damper sums),
    - lmaxold, nmaxold: max azimuthal mode number of max radial mode number (radial mode begins at 0)
          already computed (in matdamperold and matZold),
    - matdamperold ,matZold: damper and impedance matrices already computed,
    - distribution: kind of transverse distribution ('gaussian' or 'parabolic'),
    - kini: number of eigenvalues of the linear system (without Landau damping)
    to initialize the solving algorithm.
    '''

    eigenvalold=np.ones(kmax)*(1.e50+1j*1.e50)
    err=1e50
    lmax=1
    nmax=1

    # absolute errors tolerated in damper and impedance sums computations
    absepsd=abseps/max(abs(coefdamper),10)# print("absepsd=",absepsd)
    absepsZ=abseps/max(abs(coefZ.real),10)# print("absepsZ=",absepsZ)

    flagcrit=True

    while flagcrit:

        #t1=ti.time()

        matdamper=compute_damper_matrix(lmax,nmax,nx,M,omegaksi,omega0,tunefrac,a,b,taub,g,
                flagdamperimp=flagdamperimp,d=d,freqd=freqd,abseps=absepsd,lmaxold=lmaxold,
                nmaxold=nmaxold,damperold=matdamperold)

        matZ=compute_impedance_matrix(lmax,nmax,nx,M,omegaksi,omega0,tunefrac,a,b,taub,g,Z,freqZ,
                flag_trapz=flag_trapz,abseps=absepsZ,lmaxold=lmaxold,nmaxold=nmaxold,couplold=matZold)

        #t2=ti.time()
        #print("lmax=",lmax,", nmax=",nmax,", time for computing matrices [seconds]: ",t2-t1)

        if ((len(np.where(np.isinf(matdamper))[0])==0)and(len(np.where(np.isnan(matdamper))[0])==0))and((len(np.where(np.isinf(matZ))[0])==0)and(len(np.where(np.isnan(matZ))[0])==0)):

            if (lmax<16)and(nmax<16):
                eigenval=solve_determinantDELPHI_tunespread(lmax,nmax,matdamper,matZ,coefdamper,coefZ,
                        bx,bxy,omega0,omegas,distribution=distribution,kini=kini)
            else:
                print(" warning: eigenmodesDELPHI_tunespread_converged: too big matrices, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))

        else:
            print("     warning: eigenmodesDELPHI_tunespread_converged: some nan or inf in matrix, convergence stopped, error={}, lmax={}, nmax={}".format(err,lmax,nmax))
            break

        ind=np.argsort(np.imag(eigenval))

        kmaxtmp=min(kmax,len(eigenval))

        try:
            err=np.max(np.abs(np.imag(eigenval[ind[:kmaxtmp]])
                -np.imag(eigenvalold[:kmaxtmp]))
                /(np.abs(np.imag(eigenvalold[:kmaxtmp]))+abseps))
        except ValueError:
            pass

        flagcrit=(err>crit)or(kmaxtmp<kmax)

        for k in range(min(kmax,len(eigenval))): eigenvalold[k]=eigenval[ind[k]]

        if (lmax>lmaxold)and(nmax>nmaxold):
            lmaxold=lmax
            nmaxold=nmax
            matZold=matZ
            matdamperold=matdamper

        lmax+=1
        nmax+=1


    return eigenval[ind],lmaxold,nmaxold,matdamperold,matZold


def eigenmodesDELPHI_tunespread_converged_scan(Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,
        bxscan,bxyscan,M,omega0,Q,gamma,eta,a,b,taub,g,Z,freq,particle='proton',flagnorm=0,
        flag_trapz=0,flagdamperimp=0,d=None,freqd=None,kmax=1,kmaxplot=10,
        crit=5.e-2,abseps=1.e-3,flagm0=False,distribution='gaussian',kini=12):

    ''' encapsulate DELPHI calculations (with tunespread), with scans on coupled-bunch modes,
    damper gain, nb of particles, synchrotron tune, damper phase, and detuning coefficients.
    return tuneshifts (complex) for all these parameters scanned
    and also the tuneshifts of the most unstable coupled-bunch modes
    Note: bxscan & bxyscan are done simultaneously if they are of same length
    (then in the resulting table of tuneshifts, the length of bxyscan is
    replaced by 1)
    '''

    f0=omega0/(2.*np.pi)
    Qfrac=Q-np.floor(Q)

    if (len(bxscan)==len(bxyscan)):
        bxyscanbis=np.array([0.])# 1D scan simultaneously along bxscan and bxyscan; only the length of bxyscanbis is used then
    else:
        bxyscanbis=bxyscan # 2D scan of bxscan and bxyscan

    if flagnorm==0:
        # normalization factor for damper
        dnormfactor=compute_damper_matrix(0,0,0,M,0.,omega0,Qfrac,a,b,taub,g,
            flagdamperimp=flagdamperimp,d=d,freqd=freqd,abseps=abseps)
        dnormfactor=2.*np.pi*dnormfactor[0,0,0,0]

    tuneshiftnx=np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis),kmaxplot),dtype=complex)
    tuneshift_most=np.zeros((len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis),kmaxplot),dtype=complex)
    if flagm0:
        tuneshiftm0nx=np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis)),dtype=complex)
        tuneshiftm0=np.zeros((len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis)),dtype=complex)

    for iQp,Qp in enumerate(Qpscan):

        omegaksi=Qp*omega0/eta

        for inx,nx in enumerate(nxscan):

            if flagnorm:
                # normalization factor for damper
                dnormfactor=compute_damper_matrix(0,0,nx,M,omegaksi,omega0,Qfrac,a,b,taub,g,
                    flagdamperimp=flagdamperimp,d=d,freqd=freqd,abseps=abseps)
                dnormfactor=2.*np.pi*dnormfactor[0,0,0,0]

            lmaxi=-1
            nmaxi=-1 # to keep track of maximum lmax and nmax for all modes
            lmax=-1
            nmax=-1
            matZ=None
            matdamper=None

            for idamp,damp in enumerate(dampscan):

                for iNb,Nb in enumerate(Nbscan):

                    for iomegas,omegas in enumerate(omegasscan):

                        for idphase,dphase in enumerate(dphasescan):

                            #print("Nb=",Nb,',lmax=',lmax,', nmax=',nmax)

                            coefdamper,coefZ=computes_coef(f0,damp,b,g[0],dnormfactor,taub,
                                    dphase,M,Nb,gamma,Q,particle=particle)

                            for ibx,bx in enumerate(bxscan):

                                for ibxy,bxy in enumerate(bxyscanbis):

                                    # when bxscan and bxyscan have same length, we scan these
                                    # 2 tables simultaneously
                                    if (len(bxscan)==len(bxyscan)): 
                                        bxy=bxyscan[ibx]

                                    freqshift,lmax,nmax,matdamper,matZ=eigenmodesDELPHI_tunespread_converged(
                                        nx,M,omegaksi,omega0,Qfrac,a,b,taub,g,Z,freq,coefdamper,coefZ,
                                        omegas,bx,bxy,flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,
                                        freqd=freqd,kmax=kmax,crit=crit,abseps=abseps,lmaxold=lmax,
                                        nmaxold=nmax,matdamperold=matdamper,matZold=matZ,
                                        distribution=distribution,kini=kini)

                                    if flagm0:
                                        # extract mode m=0 tuneshift
                                        tuneshiftm0nx[iQp,inx,idamp,iNb,iomegas,idphase,ibx,ibxy]=extract_between_bounds(freqshift/omega0,-omegas/(2.*omega0),omegas/(2.*omega0))

                                    if (len(freqshift)>=kmaxplot):
                                        tuneshiftnx[iQp,inx,idamp,iNb,iomegas,idphase,ibx,ibxy,:]=freqshift[:kmaxplot]/omega0
                                    else:
                                        tuneshiftnx[iQp,inx,idamp,iNb,iomegas,idphase,ibx,ibxy,:len(freqshift)]=freqshift[:]/omega0

                                    #lambdax[iNb,:]=freqshift[:kmaxplot]/omegas
                                    lmaxi=max(lmax,lmaxi)
                                    nmaxi=max(nmax,nmaxi)

        # find the most unstable coupled-bunch mode
        for idamp,damp in enumerate(dampscan):

            for iNb,Nb in enumerate(Nbscan):

                for iomegas,omegas in enumerate(omegasscan):

                    for idphase,dphase in enumerate(dphasescan):

                        for ibx,bx in enumerate(bxscan):

                            for ibxy,bxy in enumerate(bxyscanbis):

                                if (len(bxscan)==len(bxyscan)):
                                    bxy=bxyscan[ibx]

                                for kmode in range(kmaxplot):

                                    inx=np.argmin(np.imag(tuneshiftnx[iQp,:,idamp,iNb,iomegas,idphase,ibx,ibxy,kmode]))
                                    if kmode<kmax:
                                        print("Qp={}, M={}, d={}, Nb={}, omegas={}, dphase={}, bx={}, bxy={}".format(Qp,M,damp,Nb,omegas,dphase,bx,bxy))
                                        print("   lmaxi={}, nmaxi={}, kmode={}, Most unstable coupled-bunch mode: {}".format(lmaxi,nmaxi,kmode,nxscan[inx]))
                                    tuneshift_most[iQp,idamp,iNb,iomegas,idphase,ibx,ibxy,kmode]=tuneshiftnx[iQp,inx,idamp,iNb,iomegas,idphase,ibx,ibxy,kmode]

                                if flagm0:

                                    inx=np.argmax(np.abs(np.real(tuneshiftm0nx[iQp,:,idamp,iNb,iomegas,idphase,ibx,ibxy])))
                                    if kmode<kmax:
                                        print("Qp={}, M={}, d={}, Nb={}, omegas={}, dphase={}, bx={}, bxy={}".format(Qp,M,damp,Nb,omegas,dphase,bx,bxy))
                                        print("   lmaxi={}, nmaxi={}, kmode={}, coupled-bunch mode with highest tune shift: {}".format(lmaxi,nmaxi,kmode,nxscan[inx]))
                                    tuneshiftm0[iQp,idamp,iNb,iomegas,idphase,ibx,ibxy]=tuneshiftm0nx[iQp,inx,idamp,iNb,iomegas,idphase,ibx,ibxy]

    if flagm0:
        return tuneshift_most,tuneshiftnx,tuneshiftm0
    else:
        return tuneshift_most,tuneshiftnx


def eigenmodesDELPHI_tunespread_converged_scan_lxplus(Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,
        bxscan,bxyscan,M,omega0,Q,gamma,eta,a,b,taub,g,Z,freq,particle='proton',
        flagnorm=0,flag_trapz=0,flagdamperimp=0,d=None,freqd=None,
        kmax=1,kmaxplot=10,crit=5.e-2,abseps=1.e-3,flagm0=False,
        distribution='gaussian',kini=12,
        lxplusbatch=None,comment='',queue='1nh',dire='',**kwargs):

    ''' same as eigenmodesDELPHI_tunespread_converged_scan with possibility to launch on lxplus
    lxplusbatch: if None, no use of lxplus batch system
                 if 'launch' -> launch calculation on lxplus on queue 'queue'
                 if 'retrieve' -> retrieve outputs
    comment is used to identify the batch job name and the pickle filename
    dire is the directory where to put/find the result; it should be a path
    relative to the current directory (e.g. '../') '''

    import pickle as pick

    if (len(bxscan)==len(bxyscan)):
        bxyscanbis=np.array([0.])# 1D scan simultaneously along bxscan and bxyscan; only the length of bxyscanbis is used then
    else:
        bxyscanbis=bxyscan # 2D scan of bxscan and bxyscan

    tuneshiftnx=np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis),kmaxplot),dtype=complex)
    tuneshift_most=np.zeros((len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis),kmaxplot),dtype=complex)
    if flagm0:
        tuneshiftm0=np.zeros((len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis)),dtype=complex)

    if (lxplusbatch==None):
        if flagm0:
            tuneshift_most,tuneshiftnx,tuneshiftm0=eigenmodesDELPHI_tunespread_converged_scan(
                Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,
                omega0,Q,gamma,eta,a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,
                distribution=distribution,kini=kini,**kwargs)

        else:
            tuneshift_most,tuneshiftnx=eigenmodesDELPHI_tunespread_converged_scan(
                Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,
                omega0,Q,gamma,eta,a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,
                distribution=distribution,kini=kini,**kwargs)

    else:

        root_filename = "DELPHI_pickle_"+comment
        input_filename = root_filename+'.npz'
        output_filename = 'out'+root_filename+'.npz'
        status, lsres = getstatusoutput('ls '+os.path.join(dire,output_filename))
        # print(status,lsres)

        # Launch jobs on HTCondor
        # Test first if output files are not already there
        if lxplusbatch.startswith('launch') and (status!=0):

            # Get the DELPHI script path
            for ii in range(0,len(sys.path)):
                if 'DELPHI_Python' in sys.path[ii]:
                    exec_path = sys.path[ii]

            # pickle file (now replaced by a numpy compressed file)
            param_dict = {'Qpscan': Qpscan, 'nxscan': nxscan, 'dampscan': dampscan,
                          'Nbscan': Nbscan, 'omegasscan': omegasscan,
                          'dphasescan': dphasescan,'bxscan': bxscan, 'bxyscan': bxyscan,
                          'M': M, 'omega0': omega0, 'Q': Q, 'gamma': gamma,
                          'eta': eta, 'a': a, 'b': b, 'taub': taub, 'g': g,
                          'Z': Z, 'freq': freq, 'particle': particle,
                          'flagnorm': flagnorm, 'flag_trapz': flag_trapz,
                          'flagdamperimp': flagdamperimp, 'd': d, 'freqd': freqd,
                          'kmax': kmax, 'kmaxplot': kmaxplot, 'crit': crit,
                          'abseps': abseps, 'flagm0': flagm0,
                          'distribution': distribution, 'kini': kini,
                          }

            script_name = 'DELPHI_tunespread_script.py'
            with open(os.path.join(exec_path,script_name),'r') as f:
                script_txt = f.read()

            _, python_exe = getstatusoutput("which python")
            exec_name = script_name.replace('.py','{}.py'.format(python_exe.replace('/','_')))
            print("Creating script {}".format(exec_name))
            with open(os.path.join(exec_path,exec_name),'w') as f:
                f.write("#!{}\n\n".format(python_exe))
                f.write(script_txt)
            
            param_dict.update(kwargs)
            np.savez_compressed(input_filename,**param_dict)

            # Select HTCondor JobFlavour equivalent to LSF queue
            if queue in ['8nm','1nh','8nh','1nd']:
                job_flavour = 'tomorrow'
            elif queue in ['2nd', '1nw','2nw']:
                job_flavour = 'nextweek'
            else:
                print('Error in LSF queue, cannot find HTCondor equivalent, use nextweek as default')
                job_flavour = 'nextweek'

            # Write the submission file and submit it to HTCondor
            # and write a script to move the data to the right folder
            filejob = open('batch'+comment+'.job','w')
            file_executable = open('move_output'+comment+'.sh', 'w')
            here = os.getcwd()

            file_executable.write("#!/bin/bash\n")
            file_executable.write("\n")
            file_executable.write("if [ -f "+output_filename+" ]; then\n")
            file_executable.write("  mv "+output_filename+" "+dire+"\n")
            file_executable.write("  mv "+os.path.join(here,input_filename)+" "+dire+"\n")
            file_executable.write("fi\n")
            file_executable.close()
            os.system('chmod 744 move_output'+comment+'.sh')

            filejob.write("executable = "+os.path.join(exec_path,exec_name)+"\n")
            filejob.write("arguments = "+input_filename+"\n")
            filejob.write("ID = $(Cluster).$(Process)\n")
            filejob.write("output = "+os.path.join(dire,"out_"+comment)+"\n")
            filejob.write("error = HTCondor_job_$(ID).err\n")
            filejob.write("log = HTCondor_job_$(Cluster).log\n")
            filejob.write("universe = vanilla\n")
            filejob.write("getenv = true\n")
            filejob.write("should_transfer_files = YES\n")
            filejob.write("transfer_input_files = "+os.path.join(here,input_filename)+", move_output"+comment+".sh\n")
            filejob.write("when_to_transfer_output = ON_EXIT\n")
            filejob.write("+JobFlavour =\""+job_flavour+"\"\n")
            filejob.write("+PostCmd = \"move_output"+comment+".sh\"\n")
            filejob.write("queue\n")
            filejob.close()
            os.system("condor_submit batch"+comment+".job")

        # Retrieve the jobs from HTCondor
        else:
            status, lsres = getstatusoutput('ls '+os.path.join(dire,output_filename))
            # print(status, lsres)

            # Calculation went fine
            # Pickle the output files
            if status == 0:
                fileall = open(os.path.join(dire,output_filename), 'r')
                tuneshift_most = pick.load(fileall)
                tuneshiftnx = pick.load(fileall)
                if flagm0 is True:
                    tuneshiftm0=pick.load(fileall)
                fileall.close()

            # Problem during calculation
            # Fill tables with nans and show a warning
            else:
                tuneshift_most.fill(np.nan+1j*np.nan)
                tuneshiftnx.fill(np.nan+1j*np.nan)
                if flagm0 is True:
                    tuneshiftm0.fill(np.nan+1j*np.nan)
                print("WARNING !!! no file"+os.path.join(dire,output_filename))

            os.system("rm -f "+input_filename+" batch"+comment+".job move_output"+comment+".sh")

    if flagm0:
        return tuneshift_most,tuneshiftnx,tuneshiftm0
    else:
        return tuneshift_most,tuneshiftnx


def DELPHI_tunespread_wrapper(imp_mod,Mscan,Qpscan,dampscan,Nbscan,omegasscan,dphasescan,
        bxscan,bxyscan,omega0,Qx,Qy,gamma,eta,a,b,taub,g,planes=['x','y'],nevery=1,particle='proton',flagnorm=0,
        flagdamperimp=0,d=None,freqd=None,
        kmax=1,kmaxplot=10,crit=5.e-2,abseps=1.e-3,flagm0=False,
        distribution='gaussian',kini=12,
        lxplusbatch=None,comment='',queue='1nh',dire='',flagQpscan_outside=True,**kwargs):

    ''' wrapper of eigenmodesDELPHI_tunespread_converged_scan_lxplus, with more scans.
    imp_mod is an impedance or wake model (see Impedance.py)
    from which one extract the planes given in 'planes'
    Mscan is the scan in number of bunches
    lxplusbatch: if None, no use of lxplus batch system
                 if 'launch' -> launch calculation on lxplus on queue 'queue'
                 if 'retrieve' -> retrieve outputs
    comment is used to identify the batch job name and the pickle filename
    dire is the directory where to put/find the result; it should be a path
    relative to the current directory (e.g. '../')
    nevery indicates the downsampling (we take 1 frequency every "nevery"
    frequencies), for the impedance.
    flagQpscan_outside: True to put the Qpscan outside the lxplus batch job,
    False so that it is inside the lxplus batch job.
    '''

    from Impedance import test_impedance_wake_comp,impedance_wake

    strnorm=['','_norm_current_chroma']

    if (len(bxscan)==len(bxyscan)):
        bxyscanbis=np.array([0.])# 1D scan simultaneously along bxscan and bxyscan; only the length of bxyscanbis is used then
    else:
        bxyscanbis=bxyscan # 2D scan of bxscan and bxyscan

    tuneshiftQp=np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis),kmaxplot),dtype=complex)
    if flagm0:
        tuneshiftm0Qp=np.zeros((len(planes),len(Mscan),len(Qpscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis)),dtype=complex)

    for iplane,plane in enumerate(planes):

        # select Zxdip or Zydip
        flagx=int(plane=='x')
        for iw in imp_mod:
            if test_impedance_wake_comp(iw,flagx,1-flagx,0,0,plane):
                Z=deepcopy(iw.func[::nevery,:])
                freq=deepcopy(iw.var[::nevery])

        for iM,M in enumerate(Mscan):

            flag_trapz=0 # by default no trapz method

            if (M==1):
                nxscan=np.array([0])
                flag_trapz=1
            else:
                nxscan=sort_and_delete_duplicates(np.concatenate((np.arange(0,M,M/20),np.arange(M/2-10,M/2+11),
                                            np.arange(M-10,M),np.arange(0,10))))
                print("number of coupled-bunch modes={}".format(len(nxscan)))

            if flagQpscan_outside:
                # put loop on Qpscan outside the lxplus job
                for iQp,Qp in enumerate(Qpscan):
                    tuneshiftnx=np.zeros((1,len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscan),kmaxplot),dtype=complex)
                    if flagm0:
                        tuneshiftQp[iplane,iM,iQp,:,:,:,:,:,:,:],tuneshiftnx,tuneshiftm0Qp[iplane,iM,iQp,:,:,:,:,:,:]=eigenmodesDELPHI_tunespread_converged_scan_lxplus(
                            [Qp],nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,omega0,eval('Q'+plane),gamma,eta,
                            a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                            flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                            kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,
                            distribution=distribution,kini=kini,
                            lxplusbatch=lxplusbatch,comment=comment+'_'+str(M)+'b_Qp'+str(Qp)+strnorm[flagnorm]+'_'+plane,
                            queue=queue,dire=dire,**kwargs)

                    else:
                        tuneshiftQp[iplane,iM,iQp,:,:,:,:,:,:,:],tuneshiftnx=eigenmodesDELPHI_tunespread_converged_scan_lxplus(
                            [Qp],nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,omega0,eval('Q'+plane),gamma,eta,
                            a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                            flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                            kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,
                            distribution=distribution,kini=kini,
                            lxplusbatch=lxplusbatch,comment=comment+'_'+str(M)+'b_Qp'+str(Qp)+strnorm[flagnorm]+'_'+plane,
                            queue=queue,dire=dire,**kwargs)

            else:
                # put loop on Qpscan inside the lxplus job
                tuneshiftnx=np.zeros((len(Qpscan),len(nxscan),len(dampscan),len(Nbscan),len(omegasscan),len(dphasescan),len(bxscan),len(bxyscanbis),kmaxplot),dtype=complex)
                if flagm0:
                    tuneshiftQp[iplane,iM,:,:,:,:,:,:,:,:],tuneshiftnx,tuneshiftm0Qp[iplane,iM,:,:,:,:,:,:,:]=eigenmodesDELPHI_tunespread_converged_scan_lxplus(
                        Qpscan,nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,omega0,eval('Q'+plane),gamma,eta,
                        a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                        flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                        kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,
                        distribution=distribution,kini=kini,
                        lxplusbatch=lxplusbatch,comment=comment+'_'+str(M)+'b'+strnorm[flagnorm]+'_'+plane,
                        queue=queue,dire=dire,**kwargs)

                else:
                    tuneshiftQp[iplane,iM,:,:,:,:,:,:,:,:],tuneshiftnx=eigenmodesDELPHI_tunespread_converged_scan_lxplus(Qpscan,
                        nxscan,dampscan,Nbscan,omegasscan,dphasescan,bxscan,bxyscan,M,omega0,eval('Q'+plane),gamma,eta,
                        a,b,taub,g,Z,freq,particle=particle,flagnorm=flagnorm,
                        flag_trapz=flag_trapz,flagdamperimp=flagdamperimp,d=d,freqd=freqd,
                        kmax=kmax,kmaxplot=kmaxplot,crit=crit,abseps=abseps,flagm0=flagm0,
                        distribution=distribution,kini=kini,
                        lxplusbatch=lxplusbatch,comment=comment+'_'+str(M)+'b'+strnorm[flagnorm]+'_'+plane,
                        queue=queue,dire=dire,**kwargs)


    if flagm0:
        return tuneshiftQp,tuneshiftm0Qp
    else:
        return tuneshiftQp


def extract_between_bounds(tuneshifts,lower,upper):
    ''' extract in a table of complex tuneshifts the "biggest" mode
    that has a real tuneshift between 'lower' and 'upper' '''

    ind=np.where((tuneshifts.real>lower)*(tuneshifts.real<upper))[0]
    #print(ind,tuneshifts)

    if (len(ind)>0):
        i=np.argmax(np.abs(np.real(tuneshifts[ind])))
        #print(i,ind[i],tuneshifts[ind[i]])
        return tuneshifts[ind[i]]

    else:
        print("Extract_between_bounds: no modes between bounds !")
        return 0


def longdistribution_decomp(taub,typelong='Gaussian'):
    ''' decomposition over Laguerre polynomials of the longitudinal distribution
    for a bunch of length taub (seconds) (4*RMS for Gaussian)
    typelong can be "Gaussian", "parabolicamp", "parabolicline", "airbag"
    '''

    from scipy import special

    if typelong.startswith('Gaussian'):
        g = np.ones(1)*8/(np.pi*taub*taub)
        a = 8.0
        b = 8.0

    else:

        # Interval for the distribution
        ntau = 10000
        delta = taub/ntau

        if typelong.startswith('parabolicline'):
            tau = np.arange(0, taub/2, delta)
            # Interval for the sampled distribution
            tau2 = np.arange(0, taub+taub/1000,taub/1000)

            distrib = 6.0/(np.pi*taub**2) * np.sqrt(1.0 - (2*tau/taub)**2)
            a = 9.0
            b = 9.0
            crit = 5e-2#10.0e-2

        if typelong.startswith('parabolicamp'):
            tau = np.arange(0,taub/2,delta)
            # Interval for the sampled distribution
            tau2 = np.arange(0,taub+taub/1000,taub/1000)

            distrib = 8.0/(np.pi*taub**2) * (1.0 - (2.0*tau/taub)**2)
            a = 4.0
            b = 4.0
            crit = 10.0e-2

        if typelong.startswith('uniform'):
            tau = np.arange(0, 2*taub,delta)
            # Interval for the sampled distribution
            tau2 = np.arange(0, 2*taub+taub/1000, taub/1000)

            distrib = (4.0/(np.pi*taub**2)) / (1 + np.exp(25.0*(tau-0.5*taub)/taub))
            a = 8.0
            b = 8.0
            crit = 5.0e-2

        if typelong.startswith('airbag'):
            tau = np.arange(0, 2*taub, delta)
            # Interval for the sampled distribution
            tau2 = np.arange(0, 2*taub+taub/1000, taub/1000)

            distrib0 = np.exp(-((taub/2 - tau)/(taub/10))**2)
            norm_coeff = 1/(2*np.pi*np.trapz(distrib0*tau, tau))
            distrib = norm_coeff*distrib0
            a = 8.0
            b = 8.0
            crit = 20.0e-2

        distribnorm = np.exp(b*(tau/taub)**2)*distrib

        i = 0
        coeff = []
        approx = np.zeros(len(tau2))

        while np.trapz(np.abs(np.interp(tau2,tau,distrib)-approx),tau2)/np.trapz(distrib,tau) > crit:

            coeff.append(np.trapz(distribnorm*np.exp(-a*(tau/taub)**2)*special.eval_genlaguerre(i,0,a*(tau/taub)**2),x=a*(tau/taub)**2))
            approx += coeff[i]*special.eval_genlaguerre(i,0,a*(tau2/taub)**2)*np.exp(-b*(tau2/taub)**2)
            i+=1
            #print(i,np.trapz(np.abs(np.interp(tau2,tau,distrib)-approx),tau2)/np.trapz(distrib,tau))

        g = np.array(coeff)

    return g,a,b


def plot_TMCI(Nbscan,lambdax,ax,part='real',leg='',patcol='xb',
        xlab='Nb particles per bunch',title='',ms=15.,ylim=[-5,5]):

    ''' TMCI plot from lambdax=(Q-Q0)/Qs of all (radial+azimuthal) coherent
    modes vs intensity Nbscan
    part = 'real' or 'imag'
    '''

    from plot_lib import plot,init_figure,end_figure

    # make a TMCI kind of plot
    sgn=1
    sgnstr=''
    if (part.startswith('real')):
        strpart='Re'
    if (part.startswith('imag')):
        strpart='Im'
        sgn=-1
        sgnstr='-' # invert sign of imaginary part

    if (lambdax.ndim>=2):
        lamsort=sgn*np.sort(getattr(lambdax,part))
        plot(Nbscan,lamsort[:,:-1],'',patcol,sgnstr+" $ "+strpart+"(Q-Q_0)/Q_s $ ",ax,0,xlab=xlab,ms=ms)
    else:
        lamsort=np.zeros((len(lambdax),1),dtype=complex)
        lamsort[:,0]=sgn*getattr(lambdax,part)

    plot(Nbscan,lamsort[:,-1],leg,patcol,sgnstr+" $ "+strpart+"(Q-Q_0)/Q_s $ ",ax,0,xlab=xlab,ms=ms)

    ax.set_title(title)
    ax.set_ylim(ylim)


def find_intensity_threshold(Nbscan,freqshift,thresgrowth=0.,ninterp=1e4):

    ''' find intensity threshold of instability
    when growth rate becomes more than thresgrowth'''

    # we reinterpolate on a finer mesh
    delta=(Nbscan[-1]-Nbscan[0])/ninterp
    x=np.arange(Nbscan[0],Nbscan[-1]+delta,delta)
    y=np.interp(x,Nbscan,np.imag(freqshift))
    
    # find threshold
    ind=np.where(y<-thresgrowth)[0]
    if (len(ind)==0):
        return Nbscan[-1]
    else: 
        return x[ind[0]]


def MOSES_wrapper(rootname,Rres,fres,Qres,Qpscan,Nbscan,omegasscan,omega0,E,alphap,sigmaz,avbeta,
        lmax,nmax,mmin=-3,mmax=3,taumin=-0.5,taumax=0.5,firstline='MOSES input file #',action='launch',
        MOSES_exec='~/DFS/Documents/MOSES4W/MOSES\ application\ for\ Windows/MOSES4W.exe',
        direname_local='DFS',dirname_remote=r"//cernhomeN.cern.ch/"+user[0]+'/'+user,flag_win=True):

    ''' Wrapper to produce easily input files for MOSES (https://oraweb.cern.ch/pls/hhh/code_website.disp_code?code_name=MOSES), from scanned parameters.
    MOSES is a code to compute instabilities, written by Y. Chin (Y. H. Chin. User's Guide for New MOSES Version 2.0 ,
    CERN/LEP-TH/88-05, 1988) that works in a similar way as DELPHI (single-bunch, resonator impedance only, with Landau damping).
    You need to have installed the executable in the path 'MOSES_exec'. This is specially
    designed for a MOSES executable working under Windows, in a directory accessible from here (typically, on a mounted
    DFS file system).

    This wrapper, depending on 'action', either writes MOSES input files for scanned parameters and write a Windows batch file
    to launch all the calculations (you still have to launch calculations by hand), or
    retrieve the MOSES output to get tuneshifts.
    Inputs:
     - rootname: root (including path) of the final input and batch filenames,
     - Rres, fres, Qres: transverse shunt impedance, resonance frequency and quality factor
       for the resonator impedance model. Each (or all) of them can be a list of values, or a scalar.
     - Qpscan: list of chromaticity (Q') to scan,
     - Nbscan: list of number of particles to scan (actually, each value is not used, but only
       the number of values, the first and the last ones, i.e. it makes a linear sampling between
       the first and last number of particles in Nbscan, with min(len(Nbscan),120) points),
     - omegasscan: list of omegas (synchrotron angular frequency in rad/s) to scan,
     - omega0: angular revolution frequency in rad/s,
     - E: energy in eV,
     - alphap: momentum compaction factor,
     - sigmaz: bunch length in m,
     - avbeta: average beta function in m (actually R/Q),
     - lmax: maximum azimuthal mode number to consider (we go from -lmax to +lmax),
     - nmax: number of radial mode -1 (0 means 1 radial mode),
     - mmin & mmax: minimum and maximum for the y-axis of MOSES TMCI plot for the
       real part of the tune shift / Qs,
     - taumin & taumax: minimum and maximum for the y-axis of MOSES TMCI plot for the
       imaginary part of the tune shift / Qs,
     - firstline: first line of MOSES input files (some comment),
     - action: 'launch' or 'retrieve': either write input file + Win. bat file to launch
       all of them easily, or read output and give tuneshifts.
     - MOSES_exec: local path where to find MOSES' executable (Unix path).
     - direname_local: local directory name in which the remote file system is mounted (do not include
     the ~/ or /home/[username]/, just put the bare directory name or path from ~/).
     - dirname_remote: remote directory name to which direname_local corresponds (NOTE: you can keep
     the '/' like this, for Windows paths the replacement by '\' will be done automatically - see next flag).
     - flag_win: True if remote path is a Windows path; then all '/' are replaced by '\'.
    Outputs:
       all modes tuneshifts, tuneshifts of mode 0, and Iscan (arrays)'''

    e,m0,c,E0=proton_param()

    if (action=='launch'):
        # Win. batch file name and directory name to put in it
        batname=rootname+'.bat'
        fidbat=open(batname,'wb')
        # create directory if not already there, and copy MOSES executable
        n=rootname.rfind('/')
        os.system("mkdir -p "+rootname[:n])
        os.system("cp "+MOSES_exec+" "+rootname[:n])
        # directory name to put in it at each line
        n1=rootname.find(dirname_local)+len(dirname_local)
        dirname=dirname_remote+rootname[n1:]
        if flag_win:
            n2=dirname.rfind('/')
            dirname=dirname[:n2].replace("/","\ ").replace(' ','')

    # generate list of resonators
    Rreslist=create_list(Rres)
    freslist=create_list(fres,n=len(Rreslist))
    Qreslist=create_list(Qres,n=len(Rreslist))
    if (len(Rreslist)!=len(freslist))or(len(Rreslist)!=len(Qreslist)):
        print("Pb in MOSES_wrapper: length of resonator models list not all identical !")
        sys.exit()

    # revolution frequency (Hz and MHz)
    f0=omega0/(2.*np.pi)
    f0MHz=f0*1e-6

    # intensity scan parameters for MOSES in mA, and number of intensities
    Iini=1e3*e*Nbscan[0]*f0
    nstep=min(120,len(Nbscan))
    Istep=1e3*e*f0*max((Nbscan[-1]-Nbscan[0])/float(nstep-1),1e8)
    Iscan=np.arange(Iini,Iini+nstep*Istep,Istep)

    # bunch length in cm
    sigmazcm=100.*sigmaz

    # energy in GeV
    EGeV=E*1e-9

    tuneshift=np.zeros((len(Rreslist),len(Qpscan),nstep,len(omegasscan),(nmax+1)*(2*lmax+1)),dtype=complex)
    tuneshiftm0=np.zeros((len(Rreslist),len(Qpscan),nstep,len(omegasscan)),dtype=complex)

    for ires,R in enumerate(Rreslist):

        # shunt impedance in MOhm/m and resonance frequency in MHz
        RMOhm=R*1e-6
        frMHz=freslist[ires]*1e-6
        Q=Qreslist[ires]

        for iQp,Qp in enumerate(Qpscan):

            for iomegas,omegas in enumerate(omegasscan):

                # synchrotron tune
                Qs=omegas/omega0
                # input/output suffix
                suffix='_R'+float_to_str(RMOhm)+'MOhmm_fr'+float_to_str(frMHz)+'MHz_Q'+float_to_str(Q)+'_Qp'+float_to_str(Qp)+'_Qs'+float_to_str(Qs)

                if (action=='launch'):
                    # input filename
                    inputname=rootname+suffix+'.dat'
                    # write input
                    fid=open(inputname,'wb')
                    fid.write(' '+firstline+'\n')
                    fid.write(' &MPARM\n')
                    fid.write(' NUS='+fortran_str(Qs)+', ENGY='+fortran_str(EGeV)+', SGMZ='+fortran_str(sigmazcm)+',  BETAC='+fortran_str(avbeta)+',\n')
                    fid.write(' REVFRQ='+fortran_str(f0MHz)+', ALPHA='+fortran_str(alphap)+', CHROM='+fortran_str(Qp)+', SPRD=0.0\n')
                    fid.write(' &END\n')
                    fid.write(' &CPARM\n')
                    fid.write(' CRNT='+fortran_str(Iini)+', STPC='+fortran_str(Istep)+', NCR='+str(nstep)+', NMODF='+str(-lmax)+', NMODE='+str(lmax)+', KRAD='+str(nmax)+'\n')
                    fid.write(' IPRINT=.TRUE., LPLE=.TRUE.\n')
                    fid.write(' &END\n')
                    fid.write(' &IPARM\n')
                    fid.write(' FREQ='+fortran_str(frMHz)+', RS='+fortran_str(RMOhm)+', QV='+fortran_str(Q)+'\n')
                    fid.write(' &END\n')
                    fid.write(' &HPARM\n')
                    fid.write(' MMIN='+str(mmin)+', MMAX='+str(mmax)+', TAUMIN='+fortran_str(taumin)+', TAUMAX='+fortran_str(taumax)+'\n')
                    fid.write(' &END\n')
                    fid.close()
                    # add one line to .bat file
                    fidbat.write('start /d '+dirname+' MOSES4W.exe '+rootname[n+1:]+suffix+'.dat\n')

                elif (action=='retrieve'):
                    # output filename
                    outputname=rootname+suffix+'.top'
                    # read output
                    linecur=0 # number of initial header lines

                    # real and imag. part of tuneshift
                    for ir,r in enumerate(['real','imag']):

                        linecur += 29 # number of bla-bla lines

                        for k in range((nmax+1)*(2*lmax+1)):
                            # read block (for a single mode)
                            s=read_ncol_file(outputname,ignored_rows=linecur)

                            if (np.max(np.abs(s[:,0]-Iscan))>1e-5):
                                print("Pb in MOSES_wrapper: intensity scan incorrect")
                                print(np.max(np.abs(s[:,0]-Iscan)))

                            tuneshift[ires,iQp,:,iomegas,k] += np.squeeze((1j**ir)*s[:,1]*Qs)

                            # go to next mode block
                            linecur += len(s[:,0])+1

                    for iNb,I in enumerate(Iscan):
                        # extract mode m=0 tuneshift
                        tuneshiftm0[ires,iQp,iNb,iomegas]=extract_between_bounds(tuneshift[ires,iQp,iNb,iomegas,:],-Qs/2.,Qs/2.)
                        # sort tuneshift (most unstable modes first) (Note: inverted sign for imag. part in MOSES)
                        ind=np.argsort(np.imag(tuneshift[ires,iQp,iNb,iomegas,:]))
                        tuneshiftnew=tuneshift[ires,iQp,iNb,iomegas,ind[::-1]]
                        tuneshift[ires,iQp,iNb,iomegas,:]=tuneshiftnew


    if (action=='launch'):
        fidbat.close()
        print(" Now you can execute manually the file",batname)

    return tuneshift,tuneshiftm0,Iscan*1e-3/(e*f0)


def long_matching_from_sigmaz(sigmaz,gamma,eta,Qs,R,V,h,particle='proton',flaglinear=False):

    ''' computes delta_p/p0 (sigma) and longitudinal emittance (in eV.s) for a matched bunch
    from:
     - sigmaz = bunch length [m],
     - gamma = relativistic mass factor,
     - eta = slip factor = alphap - 1/gamma^2,
     - Qs = synchrotron tune (at zero amplitude),
     - R = machine radius (circumference / 2pi) [m],
     - V = RF voltage [V],
     - h = RF harmonic number,
     - particle -> 'proton' or 'electron',
     - flaglinear -> True for linear RF bucket (otherwise it does non-linear matching).
    '''

    e,m0,c,E0=eval(particle+'_param()')
    beta=np.sqrt(1.-1./(gamma**2))
    p0=m0*beta*gamma*c
    E=gamma*E0/e # total energy in eV


    if flaglinear:
        # linear matching (based on B. Salvant Mathematica notebook)
        delta=Qs*sigmaz/(R*eta) # sigma(delta_p/p0)
        eps0=4*np.pi*p0*beta*sigmaz*delta/e # long. emittance (eV.s)

    else:
        # non-linear matching (based on E. Metral Excel sheet)
        taub=4*sigmaz/(beta*c) # total bunch length in seconds (4 times RMS)
        print("total bunch length in ns: {}".format(taub*1e9))
        f0=c*beta/(2*np.pi*R) # rev. frequency
        # long. emittance (eV.s)
        eps0=np.sqrt(V*256*E*R**2/(2*np.pi*np.abs(eta)* h**3 * c**2))*(np.sin(np.pi*h*f0*taub/2.255)/np.sin(np.pi/2.255))**2
        # RF bucket acceptance (eV.s)
        accept=eps0*(np.sin(np.pi/2.255)/np.sin(np.pi*h*f0*taub/2.255))**2
        # sigma(delta_p/p0)
        delta=np.pi*h*f0*accept*np.sin(np.pi*h*f0*taub/2)/(8*E*beta**2)


    return delta,eps0


def Qs_from_RF_param(V,h,gamma,eta,phis=0.,particle='proton'):

    ''' computes Qs (at zero amplitude) from RF parameters:
     - V = RF voltage [V],
     - h = RF harmonic number,
     - gamma = relativistic mass factor,
     - eta = slip factor = alphap - 1/gamma^2,
     - phis = synchrotron phase [rad],
     - particle -> 'proton' or 'electron'.
    '''

    e,m0,c,E0=eval(particle+'_param()')

    beta=np.sqrt(1.-1./(gamma**2))
    p0=m0*beta*gamma*c

    Qs=np.sqrt(e*V*eta*h*np.cos(phis)/(2*np.pi*beta*c*p0))

    return Qs


def eta_from_Qs_RF_param(Qs,V,h,gamma,phis=0.,particle='proton'):

    ''' computes eta (slip factor) from RF parameters:
     - Qs = synchrotron tune,
     - V = RF voltage [V],
     - h = RF harmonic number,
     - gamma = relativistic mass factor,
     - eta = slip factor = alphap - 1/gamma^2,
     - phis = synchrotron phase [rad],
     - particle -> 'proton' or 'electron'.
    '''

    e,m0,c,E0=eval(particle+'_param()')

    beta=np.sqrt(1.-1./(gamma**2))
    p0=m0*beta*gamma*c

    eta=Qs**2*2*np.pi*beta*c*p0/(e*V*h*np.cos(phis))

    return eta


def write_CFG_HEADTAIL(cfgname,particle='proton',Nb=1.e11,betax=65.976,betay=71.526,
        sigmaz=0.0937,emitx=2e-6,emity=2e-6,delta=1.4435e-4,Qs=2.34243e-3,alphap=3.225e-4,
        circ=26658.883,gamma=4263.16,nkick=1,nturns=200000,pipex=0.05,pipey=0.05,
        nsigmaz=2,Qx=64.31,Qy=59.32,Qpx=0,Qpy=0,isyn=4,nMP_per_sl=5000,nbunch=1,
        nsl=200,spacing=20,ibunchtb=0,iwake=1,ipipe=8,ntwake=20,fresT=1e9,QT=1.,
        RT=0.,fresL=200e6,QL=140.,RL=0.,condRW=1.4e6,lenRW=26658.883,ibeta=0,
        iwaketb=6,ispace=0,smooth=3,kickswitch=1,xkick=1,ykick=1.,zkick=0.,iamp=1,
        icoupl=0,coupl=0.0015,dispx=0,isext=0,sext_str=-0.254564,disp_sext=2.24,
        iloss=2,Qsecx=0,Qsecy=0,nturns_betn=1,start_turn=199000,end_turn=199100,
        VRF=12e6,h=35640,VRF2_start=0.,VRF2_end=0.,h2=18480,phase=0.,start_RF2=2000,
        end_RF2=3000,prate=0.,alphap_sec=0.,phase_shift_max=1,octfoc=0.,octdefoc=0.,
        dratex=0.02,dratey=0.02,nMP_prb=500,ipre=1,idampbeam=0):

    ''' Write an HEADTAIL input file (.cfg)
    Parameters are the same (and in the same order) as in the .cfg file, except that:
    - units are all SI except prate in GeV/c/s (but nsigmaz, xkick and ykick are still in number of sigmas,
    and spacing in RF buckets),
    - number of macroparticles=nMP_per_sl*nsl.
    cfgname contains the name of the input file (with extension).
    Default values are typical of 2012 LHC operation at 4TeV (except detuning and Qsec).
    '''

    nMP=nMP_per_sl*nsl
    emitx *= 1e6
    emity *= 1e6
    fresT /= 1e9
    fresL /= 1e6
    RT /= 1e6
    RL /= 1e6
    if particle.startswith('proton'):
        ipart=1
    elif particle.startswith('electron'):
        ipart=2

    fid=open(cfgname,'wb')
    fid.write('Flag_for_bunch_particles_(1->protons_2->positrons_3&4->ions)):   {}\n'.format(ipart))
    fid.write('Number_of_particles_per_bunch:                                  {}\n'.format(Nb))
    fid.write('Horizontal_beta_function_at_the_kick_sections_[m]:              {}\n'.format(betax))
    fid.write('Vertical_beta_function_at_the_kick_sections_[m]:                {}\n'.format(betay))
    fid.write('Bunch_length_(rms_value))_[m]:                                   {}\n'.format(sigmaz))
    fid.write('Normalized_horizontal_emittance_(rms_value))_[um]:               {}\n'.format(emitx))
    fid.write('Normalized_vertical_emittance_(rms_value))_[um]:                 {}\n'.format(emity))
    fid.write('Longitudinal_momentum_spread:                                   {}\n'.format(delta))
    fid.write('Synchrotron_tune:                                               {}\n'.format(Qs))
    fid.write('Momentum_compaction_factor:                                     {}\n'.format(alphap))
    fid.write('Ring_circumference_length_[m]:                                  {}\n'.format(circ))
    fid.write('Relativistic_gamma:                                             {}\n'.format(gamma))
    fid.write('Number_of_kick_sections:                                        {}\n'.format(nkick))
    fid.write('Number_of_turns:                                                {}\n'.format(nturns))
    fid.write('Horizontal_semiaxis_of_beam_pipe_[m]                            {}\n'.format(pipex))
    fid.write('Vertical_semiaxis_of_beam_pipe_[m]                              {}\n'.format(pipey))
    fid.write('Longitud_extension_of_the_bunch_(+/-N*sigma_z))                  {}\n'.format(nsigmaz))
    fid.write('Horizontal_tune:                                                {}\n'.format(Qx))
    fid.write('Vertical_tune:                                                  {}\n'.format(Qy))
    fid.write("Horizontal_chromaticity_[Q'x]:                                  {}\n".format(Qpx))
    fid.write("Vertical_chromaticity_[Q'y]:                                    {}\n".format(Qpy))
    fid.write('Flag_for_synchrotron_motion:                                    {}\n'.format(isyn))
    fid.write('Number_of_macroparticles_per_bunch:                             {}\n'.format(nMP))
    fid.write('Number_of_bunches:                                              {}\n'.format(nbunch))
    fid.write('Number_of_slices_in_each_bunch:                                 {}\n'.format(nsl))
    fid.write('Spacing_between_consecutive_bunches_centroids_[RF_bkts]:        {}\n'.format(spacing))
    fid.write('Switch_for_bunch_table:                                         {}\n'.format(ibunchtb))
    fid.write('Switch_for_wake_fields:                                         {}\n'.format(iwake))
    fid.write('Switch_for_pipe_geometry_(0->round_1->flat)):                    {}\n'.format(ipipe))
    fid.write('Number_of_turns_for_the_wake:                                   {}\n'.format(ntwake))
    fid.write('Res_frequency_of_broad_band_resonator_[GHz]:                    {}\n'.format(fresT))
    fid.write('Transverse_quality_factor:                                      {}\n'.format(QT))
    fid.write('Transverse_shunt_impedance_[MOhm/m]:                            {}\n'.format(RT))
    fid.write('Res_frequency_of_longitudinal_resonator_[MHz]:                  {}\n'.format(fresL))
    fid.write('Longitudinal_quality_factor:                                    {}\n'.format(QL))
    fid.write('Longitudinal_shunt_impedance_[MOhm]:                            {}\n'.format(RL))
    fid.write('Conductivity_of_the_resistive_wall_[1/Ohm/m]:                   {}\n'.format(condRW))
    fid.write('Length_of_the_resistive_wall_[m]:                               {}\n'.format(lenRW))
    fid.write('Switch_for_beta:                                                {}\n'.format(ibeta))
    fid.write('Switch_for_wake_table:                                          {}\n'.format(iwaketb))
    fid.write('Flag_for_the_transverse_space_charge:                           {}\n'.format(ispace))
    fid.write('Smoothing_order_for_longitudinal_space_charge:                  {}\n'.format(smooth))
    fid.write('Switch_for_initial_kick:                                        {}\n'.format(kickswitch))
    fid.write('x-kick_amplitude_at_t=0_[sigmas]:                               {}\n'.format(xkick))
    fid.write('y-kick_amplitude_at_t=0_[sigmas]:                               {}\n'.format(ykick))
    fid.write('z-kick_amplitude_at_t=0_[m]:                                    {}\n'.format(zkick))
    fid.write('Switch_for_amplitude_detuning:                                  {}\n'.format(iamp))
    fid.write('Linear_coupling_switch(1->on_0->off)):                           {}\n'.format(icoupl))
    fid.write('Linear_coupling_coefficient_[1/m]:                              {}\n'.format(coupl))
    fid.write('Average_dispersion_function_in_the_ring_[m]:                    {}\n'.format(dispx))
    fid.write('Sextupolar_kick_switch(1->on_0->off)):                           {}\n'.format(isext))
    fid.write('Sextupole_strength_[1/m^2]:                                     {}\n'.format(sext_str))
    fid.write('Dispersion_at_the_sextupoles_[m]:                               {}\n'.format(disp_sext))
    fid.write('Switch_for_losses_(0->no_losses_1->losses)):                     {}\n'.format(iloss))
    fid.write("Second_order_horizontal_chromaticity_(Qx'')):                    {}\n".format(Qsecx))
    fid.write("Second_order_vertical_chromaticity_(Qy'')):                      {}\n".format(Qsecy))
    fid.write('Number_of_turns_between_two_bunch_shape_acquisitions:           {}\n'.format(nturns_betn))
    fid.write('Start_turn_for_bunch_shape_acquisitions:                        {}\n'.format(start_turn))
    fid.write('Last_turn_for_bunch_shape_acquisitions:                         {}\n'.format(end_turn))
    fid.write('Main_rf_voltage_[V]:                                            {}\n'.format(VRF))
    fid.write('Main_rf_harmonic_number:                                        {}\n'.format(h))
    fid.write('Initial_2nd_rf_voltage_[V]:                                     {}\n'.format(VRF2_start))
    fid.write('Final_2nd_rf_cavity_voltage_[V]:                                {}\n'.format(VRF2_end))
    fid.write('Harmonic_number_of_2nd_rf:                                      {}\n'.format(h2))
    fid.write('Relative_phase_between_cavities:                                {}\n'.format(phase))
    fid.write('Start_turn_for_2nd_rf_ramp:                                     {}\n'.format(start_RF2))
    fid.write('End_turn_for_2nd_rf_ramp:                                       {}\n'.format(end_RF2))
    fid.write('Linear_Rate_of_Change_of_Momentum_[GeV/c/sec]:                  {}\n'.format(prate))
    fid.write('Second_Order_Momentum_Compaction_Factor:                        {}\n'.format(alphap_sec))
    fid.write('Max_phase_shift_delay_after_transition_crossing_[turns]:        {}\n'.format(phase_shift_max))
    fid.write('LHC_focusing_octupoles_current_[A]:                             {}\n'.format(octfoc))
    fid.write('LHC_defocusing_octupoles_current_[A]:                           {}\n'.format(octdefoc))
    fid.write('Horizontal_damper_rate_[inverse_of_nb_damping_turns]:           {}\n'.format(dratex))
    fid.write('Vertical_damper_rate_[inverse_of_nb_damping_turns]:             {}\n'.format(dratey))
    fid.write('Number_of_macroparticles_in_prb_file:                           {}\n'.format(nMP_prb))
    fid.write('Flag_for_wake_pretreatment_(0->no_1->pretreatment))              {}\n'.format(ipre))
    fid.write('Flag_for_damping_beam_average_(1->yes_0->bunch_by_bunch_damper)) {}\n'.format(idampbeam))

    fid.close()

    return


class DELPHI_calc(object):
    ''' class for DELPHI computations (NOT USED AND NOT FINISHED)'''

    def __init__(self,machine='LHC',M=1,nx=0,omegaksi=0.,circ=26658.883,Q=64.31,a=8.,b=8.,
        taub=1.2e-9,gamma=7460.52,particle='proton',g=None,Z=None,freqZ=None,flag_trapz=0,
        flagdamperimp=0,d=None,freqd=None,kmax=5,crit=5.e-2,abseps=1.e-3,lmax=-1,nmax=-1,
        matdamper=None,matZ=None,flageigenvect=False,flagnormscan=np.array([0.]),
        dampscan=np.array([0.]),dphasescan=np.array([0.]),Nbscan=np.array([1.5e11]),
        Qsscan=np.array([2.e-3])):
        # default values typical of LHC (single-bunch) at 7 TeV

        self.machine=machine
        self.M=M
        self.nx=nx
        self.omegaksi=omegaksi
        self.circ=circ
        self.Q=Q
        self.a=a
        self.b=b
        self.taub=taub
        self.gamma=gamma
        self.particle=particle
        self.g=g
        self.Z=Z
        self.freqZ=freqZ
        self.flag_trapz=flag_trapz
        self.flagdamperimp=flagdamperimp
        self.d=d
        self.freqd=freqd
        self.kmax=kmax
        self.crit=crit
        self.abseps=abseps
        self.lmax=lmax
        self.nmax=nmax
        self.matdamper=matdamper
        self.matZ=matZ
        self.flageigenvect=flageigenvect
        self.flagnormscan=flagnormscan
        self.dampscan=dampscan
        self.dphasescan=dphasescan
        self.Nbscan=Nbscan
        self.Qsscan=Qsscan




if __name__ == "__main__":


    machine='VEPP'
    e=1.602176487e-19 # elementary charge
    m0=9.10938e-31 # electron mass in kg
    c=299792458 # speed of light
    E0=m0*c**2 # rest energy

    E=1.8e9 # injection energy=1.8 GeV
    gamma=E*e/E0
    beta=np.sqrt(1.-1./(gamma**2))
    sigmaz=7.5e-2 # RMS bunch length (m)
    bunchlength=4.*sigmaz # full length in m
    VEPPlength=beta*c/8.377e5 # total circumference in m
    circ=VEPPlength
    R=circ/(2.*np.pi) # machine radius
    taub=bunchlength/(beta*c)
    Qx=7.62
    Qs=0.025
    alphap=0.01645 # momentum compaction factor
    M=1 # number of bunches
    max_azim=2
    nrad=2
    kmax=10
    nxmin=0
    nxmax=-1
    nxadded=[]
    f0=c*beta/circ # rev. frequency

    beta1=15
    betaav=R/Qx

    Qpx=-7.5

    # parameters for intensity scan
    deltaI=0.5
    maxI=50
    deltaI_0=0.2
    maxI_0=20

    dphase=0 # additional phase applied to the damper (rad)

    # one broad band model (NB: shunt impedances to be multiplied by beta(location)/(R/Q) )
    f=np.concatenate((10.**np.arange(-1,7),np.arange(1.e7,1.001e10,1.e7),10.**np.arange(10.1,13.1,0.1),
        10.**np.arange(14,16)))
    Zx=np.zeros((len(f),2))
    #print(f,len(f))
    R1=2.5e6*beta1/betaav
    f1=R*f0*0.795/sigmaz
    Q1=1.
    Zt=(R1*f1/f)/(1.-1j*Q1*(f1/f-f/f1))
    Zx[:,0]=np.real(Zt)
    Zx[:,1]=np.imag(Zt)
    model='_1BB_Karliner-Popov'

    eta=alphap-1./(gamma*gamma)
    omegaksi=Qpx*2*np.pi*f0/eta
    g=np.zeros(1)
    a=8.
    b=8
    g[0]=8./(np.pi*taub*taub)

    # damper model
    fd=np.concatenate((np.arange(2.e4,2.002e7,2.e4),np.arange(2.1e7,1.001e9,1e6),np.arange(1.1e9,1.01e10,1.e8)))
    fd=np.concatenate((np.flipud(-fd),np.array([0.]),fd))
    Zd=np.zeros((len(fd),2))
    #print(fd,len(fd))
    L0=0.4
    L1=0.1
    L2=0.2
    tauf=800*L1/c
    print(tauf*2*np.pi*f0) # set of parameters with bare tune (including integer part)
    dimp=damper_imp_Karliner_Popov(L0,L1,L2,tauf,R,Qx,f0,fd)
    Zd[:,0]=np.real(dimp)
    Zd[:,1]=np.imag(dimp)

    #mat=compute_impedance_matrix(2,1,0,M,omegaksi,2.*np.pi*f0,Qx-np.floor(Qx),a,b,taub,g,Zx,f,1)
    mat=compute_damper_matrix(2,1,0,M,omegaksi,2.*np.pi*f0,Qx-np.floor(Qx),a,b,taub,g,Zd,fd,flagdamperimp=1)

    print(mat)

    #taub=1.0006923259e-9
    #DArray1=c_double * 1
    #g=DArray1(8/(np.pi*taub*taub))

    #prototype = CFUNCTYPE(c_double, c_int,c_int,c_double,c_double,c_double, c_long, DArray1)
    #Gln = prototype(('Gln', libDELPHI))

    #print(Gln(0,0,-1.e9,8/(taub*taub), taub, 0, g))


# example:
# arr = (c_int * len(pyarr))(*pyarr)
